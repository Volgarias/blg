//==============================================================================
// Les fonctions des rôles
//==============================================================================


//==============================================================================
// VOYANTE
//==============================================================================

/*
UTILITÉ
Joue le tour de la voyante
*/
function tourVoyante(){
  return new Promise(function(resolve){ // J'utilise les promesses parce que le tour des loups commence en même temps que la voyante sinon
    channelTextuelVillage.send("```La voyante se réveille et désigne un joueur dont elle veut sonder la véritable personnalité !```");
    let [textEmbed, lesMembres] = getListeVillageAvecLettres();
    channelTextuelVoyante.send(new Discord.MessageEmbed().setTitle("De quel joueur veut-tu connaitre le rôle ?").addField("Temps restant", tempsChoixVoyante+"s").setDescription(textEmbed).setColor(couleurEmbedSondage)).then(async function(newMessage){
      let listeEmojis = [];
      for(let i=0; i<lesMembres.length; i++){
        listeEmojis.push(lesLettres.get(i));
        await newMessage.react(lesLettres.get(i));
      }

      let cpt = 0;
      let timer = setInterval(function(){
        newMessage.edit(new Discord.MessageEmbed().setTitle("De quel joueur veut-tu connaitre le rôle ?").addField("Temps restant", (tempsChoixVoyante-cpt)+"s").setDescription(textEmbed).setColor(couleurEmbedSondage));
        if(cpt >= tempsChoixVoyante){
          clearInterval(timer);
        }
        cpt ++;
      }, 1000);

      const filter = (reaction, user) => listeEmojis.includes(reaction.emoji.name) && !user.bot; // On vérifie ue l'émojis est parmis les réactions originales
      const collector = newMessage.createReactionCollector(filter, { time: tempsChoixVoyante*1000, max: 1 }); // On arrête à la première réaction récoltée

      collector.on('end', async function(collected){ // Collected est le map des emojis collectés
        let user;
        if(collected.size > 0){
          user = lesMembres[listeEmojis.indexOf(collected.firstKey())]; // Le User correspondant à la lettre choisie
          evenementBulle("voyante.svg", "<b>La voyante</b> sonde le rôle de <b>"+user.username+"</b> ("+lesJoueurs.get(user)[0]+")"); // Pour les logs
          newMessage.edit(new Discord.MessageEmbed().setTitle(user.username));
          await expliquerRole(lesJoueurs.get(user)[0], channelTextuelVoyante);
        }else{
          evenementBulle("voyante.svg", "<b>La voyante</b> ne sonde personne cette nuit"); // Pour les logs
          newMessage.edit(new Discord.MessageEmbed().setTitle("Vous n'avez sondé personne"));
        }
        channelTextuelVillage.send("```La voyante se rendort```");
        resolve(); // Pour dire à la promesse que le travail est finit
      });
    });
  });
}

//==============================================================================
// LOUP-GAROU
//==============================================================================

/*
UTILITÉ
Joue le tour des loups-garou
*/
function tourLoupGarou(){
  return new Promise(function(resolve){
    channelTextuelVillage.send("```Les Loups-Garous se réveillent et discutent pour désigner une victime```");

    changerPermissionEcritureLoups(true); // On met les droit en écriture pour les loups garou dans leur channel textuel

    if(petiteFille){
      tourPetiteFille();
    }else{
      messagesTemporaires[1] = "<b>La Petite-Fille</b> n'est pas/plus présente dans la partie, son tour passe"; // Pour les logs
    }

    let [chaine, listePersonnes] = getListeVillageAvecLettres(true);

    channelTextuelLoupsGarous.send(new Discord.MessageEmbed().setTitle("À vous de jouer !").setDescription("Choisissez une victime : mettez-vous tous d'accord.\nLe système choisira au hasard en cas d'égalité.\n\n"+chaine).addField("Temps restant", tempsMaxDispoPourVoteLoups+"s").setFooter("Votez tous pour une personne pour valider le vote").setColor(couleurEmbedSondage)).then(async function(message){

      let listeReactions = [];
      for(let i=0; i<listePersonnes.length; i++){
        listeReactions.push(lesLettres.get(i));
      }

      let cpt = 0;
      let timer = setInterval(function(){
        message.edit(new Discord.MessageEmbed().setTitle("À vous de jouer !").setDescription("Choisissez une victime : mettez-vous tous d'accord.\nLe système choisira au hasard en cas d'égalité.\n\n"+chaine).addField("Temps restant", (tempsMaxDispoPourVoteLoups-cpt)+"s").setFooter("Votez tous pour une personne pour valider le vote").setColor(couleurEmbedSondage));
        if(cpt >= tempsMaxDispoPourVoteLoups){
          clearInterval(timer);
        }
        cpt ++;
      }, 1000);


      const filter = (reaction, user) => listeReactions.includes(reaction.emoji.name) && !user.bot;
      const collector = message.createReactionCollector(filter, { time: tempsMaxDispoPourVoteLoups*1000});

      for(let i=0; i<listePersonnes.length; i++){
        await message.react(lesLettres.get(i)); // On met une réaction pour chaque personne qui n'est pas loup
      }

      collector.on('end', async function(collected, reason){
        clearInterval(timer);
        await message.delete();

        if(collected.size == 0){
          messagesTemporaires[0] = "<b>Les Loups-Garous</b> ne choisissent personne cette nuit"; // Pour les logs
          channelTextuelLoupsGarous.send(new Discord.MessageEmbed().setTitle("Temps écoulé").setDescription("Vous n'avez finalement pas réagis donc personne ne mourra cette nuit.").setColor(couleurEmbedInfos));
        }else{
          let mort;
          if(collected.size > 1){
            mort = listePersonnes[listeReactions.indexOf(collected.randomKey())];
            messagesTemporaires[0] = "<b>Les Loups-Garous</b> ne sont pas d'accord, c'est <b>"+mort.username+"</b> ("+lesJoueurs.get(mort)[0]+") qui est désigné au hasard parmis leurs choix"; // Pour les logs
            channelTextuelLoupsGarous.send(new Discord.MessageEmbed().setTitle("Vous n'êtes pas d'accord !").setDescription("C'est donc **"+mort.username+"** qui à été choisi au hasard parmis vos choix").setColor(couleurEmbedInfos));
          }else{ // Une seule personne à été choisi
            mort = listePersonnes[listeReactions.indexOf(collected.firstKey())];
            messagesTemporaires[0] = "<b>Les Loups-Garous</b> choisissent de tuer <b>"+mort.username+"</b> ("+lesJoueurs.get(mort)[0]+")"; // Pour les logs
            channelTextuelLoupsGarous.send(new Discord.MessageEmbed().setTitle("Vous avez choisi !").setDescription("Vous avez choisi de tuer **"+mort.username+"**").setColor(couleurEmbedInfos));
          }
          mortsDeLaNuit.push(mort);
        }
        changerPermissionEcritureLoups(false);
        channelTextuelVillage.send("```Les Loups-garous ont finit de débattre !```");
        resolve();
      });
    });
  });
}

//==============================================================================
// SORCIÈRE
//==============================================================================

/*
UTILITÉ
Joue le tour de la sorciere
*/
function tourSorciere(){
  return new Promise(function(resolve){
    channelTextuelVillage.send("```La Sorcière se réveille, je lui montre la victime des Loups-Garous. Va-t-elle user de ses potions ?```");

    let [guerison, empoisonement] = listePotionsSorciere;
    if(guerison || empoisonement){ // S'il lui reste au moins une potion à utiliser

      let listeVotesPossibles = ["👍", "👊", "👎"];
      let desc = "";
      if(guerison && mortsDeLaNuit[0]){ // Les loups ont bien choisi une victime
        desc += "Les Loups-Garous ont choisi de tuer **"+mortsDeLaNuit[0].username+"**\n\n:thumbsup: Potion de guérison pour soigner la cible des Loups-Garous.\n";
      }else if(mortsDeLaNuit[0]){ // Plus la potion mais un mort
        desc += "Les Loups-Garous ont choisi de tuer **"+mortsDeLaNuit[0].username+"**\n\n:thumbsup: ~~Potion de guérison pour soigner la cible des Loups-Garous.~~\n";
      }else{ // Ni potion ni mort ou juste pas de mort
        desc += "Les Loups-Garous n'ont fait aucune victime cette nuit\n\n:thumbsup: ~~ Potion de guérison pour soigner la cible des Loups-Garous.~~\n"
      }

      desc += ":punch: Ne rien faire\n";

      if(empoisonement){
        desc += ":thumbsdown: Potion d'empoisonement pour tuer une personne de votre choix";
      }else{
        desc += ":thumbsdown: ~~Potion d'empoisonement pour tuer une personne de votre choix~~";
      }

      channelTextuelSorciere.send(new Discord.MessageEmbed().setTitle("À vous de jouer !").setDescription(desc).addField("Temps restant", tempsChoixSorciere+"s").setFooter("Vous pouvez choisir de n'utiliser aucune de vos potions\nUne fois utilisée, une potion ne sera plus utilisable.").setColor(couleurEmbedSondage)).then(async function(message){

        if(guerison && mortsDeLaNuit[0]){ // Condition pour sauver quelqu'un
          await message.react(listeVotesPossibles[0]);
        }else{
          await message.react("⛔");
        }
        await message.react(listeVotesPossibles[1]);
        if(empoisonement){
          await message.react(listeVotesPossibles[2]);
        }else{
          await message.react("⛔");
        }

        const filter = (reaction, user) => listeVotesPossibles.includes(reaction.emoji.name) && !user.bot; // On s'assure que l'emoji soit bien un des 3 acceptés et que ce ne soit pas une réaction du bot
        const collector = message.createReactionCollector(filter, { time: tempsChoixSorciere*1000 });

        let cpt = 0;
        let timer = setInterval(async function(){
          message.edit(new Discord.MessageEmbed().setTitle("À vous de jouer !").setDescription(desc).addField("Temps restant", (tempsChoixSorciere-cpt)+"s").setFooter("Vous pouvez choisir de n'utiliser aucune de vos potions\nUne fois utilisée, une potion ne sera plus utilisable.").setColor(couleurEmbedSondage));
          if(cpt >= tempsChoixSorciere){
            clearInterval(timer);
            if(! collector.ended){
              await collector.stop("time");
            }
            await channelTextuelVillage.send("```La sorcière se rendort```");
            resolve();
          }
          cpt ++;
        }, 1000);

        collector.on('collect', async function(r){
          switch (r.emoji.name){
            case listeVotesPossibles[0]: // Sauver
              evenementBulle("sorciere.svg", "<b>La sorciere</b> choisi de sauver la victime des Loups-Garous ("+mortsDeLaNuit[0].username+")"); // Pour les logs
              await channelTextuelSorciere.send(new Discord.MessageEmbed().setTitle("Vous avez choisi de sauver **"+mortsDeLaNuit[0].username+"**").setDescription("Vous ne pourrez plus utiliser votre potion de guérison à l'avenir").setColor(couleurEmbedInfos));
              listePotionsSorciere[0] = false;
              mortsDeLaNuit = [];
              collector.stop("choix");
              clearInterval(timer);
              break;

            case listeVotesPossibles[2]: // Empoisonner
              let [chaine, listePersonnes] = getListeVillageAvecLettres();
              channelTextuelSorciere.send(new Discord.MessageEmbed().setTitle("Vous avez choisi d'utiliser votre potion d'empoisonement mais sur qui ?").setDescription("Choisisez une victime dans la liste suivante\n"+chaine).setColor(couleurEmbedInfos)).then(async function(newMessage){

                let listeEmojis = [];
                for(let i=0; i<listePersonnes.length; i++){
                  listeEmojis.push(lesLettres.get(i));
                  await newMessage.react(lesLettres.get(i));
                }

                const filter2 = (reaction, user) => listeEmojis.includes(reaction.emoji.name) && !user.bot; // On vérifie que l'émojis est parmis les réactions originales
                const collector2 = newMessage.createReactionCollector(filter2, {max: 1}); // Limite fixée à 1 réaction qui ne vient pas du bot

                collector2.on('end', function(collected){ // Collected est le map des emojis collectés
                  let user = listePersonnes[listeEmojis.indexOf(collected.firstKey())]; // Le User correspondant à la lettre choisie

                  evenementBulle("sorciere.svg", "<b>La sorciere</b> choisi d'utiliser sa potion de poison sur <b>"+user.username+"</b> ("+lesJoueurs.get(user)[0]+")"); // Pour les logs
                  channelTextuelSorciere.send(new Discord.MessageEmbed().setTitle("Vous avez choisi d'empoisonner **"+user.username+"**").setDescription("Vous ne pourrez plus utiliser votre potion d'empoisonement à l'avenir").setColor(couleurEmbedInfos));
                  listePotionsSorciere[1] = false; // Enregistrer l'utilisation de la potion
                  mortsDeLaNuit.push(user); // Ajouter la personne empoisonnée dans la liste des morts de la nuit
                  newMessage.delete();
                  collector.stop("choix");
                });
              });
              break;

            case listeVotesPossibles[1]: // On ne fait rien
              evenementBulle("sorciere.svg", "<b>La sorciere</b> ne fait rien"); // Pour les logs
              channelTextuelSorciere.send(new Discord.MessageEmbed().setTitle("Vous avez choisi de ne rien faire").setColor(couleurEmbedInfos));
              collector.stop("choix");
              break;
          }
        });

        collector.on('end', function(collected, reason){
          if(reason == "time"){ // S'il n'y a pas de raison indiquant pourquoi on arrive à la fin, c'est qu'on est à court de temps
            evenementBulle("sorciere.svg", "<b>La sorciere</b> est à court de temps"); // Pour les logs
            channelTextuelSorciere.send(new Discord.MessageEmbed().setTitle("À court de temps !").setDescription("Vous êtes à court de temps malheureusement").setColor(couleurEmbedError));
          }
        });
      });
    }else{
      channelTextuelSorciere.send(new Discord.MessageEmbed().setTitle("C'est votre tour").setDescription("Vous n'avez plus aucune potion à utiliser mais pour éviter que tout le monde ne le sache, vous devez attendre "+tempsChoixSorciere+"s pour simuler votre choix").addField("Temps restant", tempsChoixSorciere+"s").setColor(couleurEmbedInfos)).then(function(newMessage){
        let cpt = 0;
        let timer = setInterval(function(){
          newMessage.edit(new Discord.MessageEmbed().setTitle("C'est votre tour").setDescription("Vous n'avez plus aucune potion à utiliser mais pour éviter que tout le monde ne le sache, vous devez attendre "+tempsChoixSorciere+"s pour simuler votre choix").addField("Temps restant", (tempsChoixSorciere-cpt)+"s").setColor(couleurEmbedInfos));
          if(cpt >= tempsChoixSorciere){
            clearInterval(timer);
            channelTextuelVillage.send("```La sorcière se rendort```");
            resolve();
          }
          cpt ++;
        }, 1000);
      });
    }
  });
}

//==============================================================================
// PETITE-FILLE
//==============================================================================

/*
UTILITÉ
Joue le tour de la petite*fillle en même temps que les loups-garous
*/
function tourPetiteFille(){
  channelTextuelPetiteFille.send(new Discord.MessageEmbed().setTitle("À vous de jouer ?").setDescription("Les Loups-Garous sont en pleine réflexion,\nVous pouvez épier leur discussion une fois par nuit pendant "+tempsObservationPetiteFille+"s.\nAppuyez sur les yeux pour les observer 👀").setFooter("Faites attention, vous avez une certaine probabilité de vous faire repérer ! Cela veut dire qu'ils recevront potentiellement des lettres de votre pseudo.").setColor(couleurEmbedSondage)).then(function(message){
    message.react("👀");

    let timer = setInterval(function(){
      if(message.reactions.cache.get("👀").count > 1){
        clearInterval(timer);

        message.delete();
        channelTextuelPetiteFille.send(new Discord.MessageEmbed().setTitle("Vous ouvrez les yeux discrètement").setDescription("Vous tendez le casque et entendez ce qu'ils se disent !").addField("Temps restant", tempsObservationPetiteFille+"s").setFooter("Si les Loups-Garous discutent, vous verrez apparaitre les messages ici.").setColor(couleurEmbedInfos)).then(function(editedMessage){

          let cpt = 0;
          let timer = setInterval(function(){
            editedMessage.edit(new Discord.MessageEmbed().setTitle("Vous ouvrez les yeux discrètement").setDescription("Vous tendez le casque et entendez ce qu'ils se disent !").addField("Temps restant", (tempsObservationPetiteFille-cpt)+"s").setFooter("Si les Loups-Garous discutent, vous verrez apparaitre les messages ici.").setColor(couleurEmbedInfos));
            if(cpt >= tempsChoixVoyante){
              clearInterval(timer);
            }
            cpt ++;
          }, 1000);

          const filter = m => m.content != "" && ! m.author.bot;
          const collector = channelTextuelLoupsGarous.createMessageCollector(filter, { time: tempsObservationPetiteFille*1000 }); // Premier paramètre à true car on ets sencé mettre un filtre qui en réalité renvoie un boolean pour ajouter ou non dans le collector le message envoyé. Ici à true, tous les messages sont acceptés
          collector.on('collect', function(m){
            channelTextuelPetiteFille.send(m.content);
          });

          collector.on('end', function(collected){
            let description = "";
            let couleur;
            if(Math.floor(Math.random() * (listeLoups.length*2)) == 0){ // 1 chance sur le double du nombre de loups restant de se faire repérer
              lettrePseudo = bot.randomItemFromListe(petiteFille.username);
              messagesTemporaires[1] = "<b>La Petite-Fille</b> choisi d'épier la discussion des Loups mais se fait repérer : la lettre <b>"+lettrePseudo+"</b> est découverte"; // Pour les logs
              channelTextuelLoupsGarous.send(new Discord.MessageEmbed().setTitle("Votre discussion a fuité").setDescription("L'un d'entre vous à aperçu une petite-fille ouvrir les yeux : \nSon nom d'utilisteur contiendrait la lettre '"+lettrePseudo+"' d'après lui.").setFooter("Prenez garde 👀").setColor(couleurEmbedError));
              description = "😞 Malheureusement, vous avez été repéré : la lettre '"+lettrePseudo+"' de votre pseudo a été découverte !";
              couleur = couleurEmbedError;
            }else{
              messagesTemporaires[1] = "<b>La Petite-Fille</b> choisi d'épier la discussion des Loups et n'est pas repérée"; // Pour les logs
              description = "😅 Ouf, vous n'avez pas été repéré cette nuit !";
              couleur = couleurEmbedInfos;
            }
            if(channelTextuelPetiteFille){
              channelTextuelPetiteFille.send(new Discord.MessageEmbed().setTitle("Bilan").setDescription(description).setColor(couleur));
            }
          });
        });
      }
    }, 1000);
  });
}

//==============================================================================
// CHASSEUR
//==============================================================================

/*
UTILITÉ
Lorsque ce rôle est tué, il peut chosir de tuer une personne parmis les joueurs dans son dernier soufle : cette fonction gère cela
*/
function tirChasseur(){
  return new Promise(async function(resolve){
    let [chaine, listePersonnes] = getListeVillageAvecLettres(false, false, true); // On retire le chasseur de la liste des personnes potentiellement tuable (il est déjà mort alors ...)

    await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Le chasseur choisi sa cible").setDescription("Les cibles possibles sont :\n\n"+chaine).addField("Temps restant", tempsEliminationChasseur+"s").setFooter("Faites feu dans le temps imprti ou je crois bien que vous allez éternuer").setColor(couleurEmbedSondage)).then(async function(messageChasseur){
      let listeEmojis = [];
      for (let j = 0; j < listePersonnes.length; j++) {
        await messageChasseur.react(lesLettres.get(j));
        listeEmojis.push(lesLettres.get(j));
      }

      let cpt = 0;
      let timer = setInterval(function(){
        messageChasseur.edit(new Discord.MessageEmbed().setTitle("Le chasseur choisi sa cible").setDescription("Les cibles possibles sont :\n\n"+chaine).addField("Temps restant", (tempsEliminationChasseur-cpt)+"s").setFooter("Faites feu dans le temps imprti ou je crois bien que vous allez éternuer").setColor(couleurEmbedSondage));
        if(cpt >= tempsEliminationChasseur){
          clearInterval(timer);
        }
        cpt ++;
      }, 1000);

      const filter = (reaction, user) => listeEmojis.includes(reaction.emoji.name && user == chasseur); // On vérifie ue l'émojis est parmis les réactions originales et que c'est le chasseur qui réagit
      const collector = messageChasseur.createReactionCollector(filter, { time: tempsEliminationChasseur*1000, max: 1 }); // Un maximum de 1 réaction

      collector.on('end', async function(collected, reason){ // Collected est le map des emojis collectés
        let user;
        if(reason == 'limit'){
          user = listePersonnes[listeEmojis.indexOf(collected.firstKey())];
          evenementBulle("mort.svg", "<b>Le chasseur | "+chasseur.username+"</b> décède et choisit d'assassiner au passage <b>"+user.username+"</b> ("+lesJoueurs.get(user)[0]+")"); // Pour les logs
          await messageChasseur.edit(new Discord.MessageEmbed().setTitle("Tir du chasseur").setDescription("Dans un dernier soupire vous abattez : **" + user.username + "**.").setColor(couleurEmbedInfos));
        }else{
          user = bot.randomItemFromListe(listePersonnes);
          evenementBulle("mort.svg", "<b>Le chasseur | "+chasseur.username+"</b> décède et assassine au hasard <b>"+user.username+"</b> ("+lesJoueurs.get(user)[0]+")"); // Pour les logs
          await messageChasseur.edit(new Discord.MessageEmbed().setTitle("Tir du chasseur").setDescription("Alors que la mort vous traverse vous tirez un coup de fusil. De façon inattendu le coup touche et tue : **" + user.username + "**.").setColor(couleurEmbedInfos));
        }

        chasseur = undefined;

        channelTextuelVillage.send(new Discord.MessageEmbed().setTitle(":skull: "+ user.username +" était... :skull:")).then(function(message){
          setTimeout(async function(){
            message.delete();
            await retirerJoueur(user);
            resolve();
          }, tempsAvantAnnonceRolePersonneElimine*1000);
        });
      });
    });
  });
}

/*
UTILITÉ
Permettre au ccapitaine, lors de sa mort, de désigner un nouveau capitaine qui prendra le relai
*/
function definirSuccesseurCapitaine(){
  return new Promise(async function(resolve){
    let [chaine, listePersonnes] = getListeVillageAvecLettres(false, true); // On retire le capitaine de la liste des successeurs possibles

    await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("À "+capitaine.username+" de choisir !").setDescription("Choisissez un successeur parmis :\n\n"+chaine).addField("Temps restant", tempsMaxChoixSuccesseur+"s").setFooter("Seul votre choix compte, à la fin du décompte, il seras validé").setColor(couleurEmbedSondage)).then(async function(message){
      let cpt = 0;
      let timer = setInterval(function(){
        message.edit(new Discord.MessageEmbed().setTitle("À "+capitaine.username+" de choisir !").setDescription("Choisissez un successeur parmis :\n\n"+chaine).addField("Temps restant", (tempsMaxChoixSuccesseur-cpt)+"s").setFooter("Seul votre choix compte, à la fin du décompte, il seras validé").setColor(couleurEmbedSondage));
        if(cpt >= tempsMaxChoixSuccesseur){
          clearInterval(timer);
        }
        cpt ++;
      }, 1000);

      let listeEmojis = [];
      for (let j = 0; j < listePersonnes.length; j++) {
        await message.react(lesLettres.get(j));
        listeEmojis.push(lesLettres.get(j));
      }

      const filter = (reaction, user) => listeEmojis.includes(reaction.emoji.name) && user == capitaine;
      const collector = message.createReactionCollector(filter, { time: tempsMaxChoixSuccesseur*1000});

      collector.on('end', async function(collected, reason){ // Collected est le map des emojis collectés
        clearInterval(timer);
        let user; // Si le capitaine ne choisis persone, user reste à undefined
        if(collected.size == 1){
          user = listePersonnes[listeEmojis.indexOf(collected.firstKey())];
        }else if(collected.size > 1){
          user = listePersonnes[listeEmojis.indexOf(collected.randomKey())];
        }

        if(!lesJoueurs.get(capitaine)[2].hasPermission("ADMINISTRATOR")){
          lesJoueurs.get(capitaine)[2].setNickname(lesJoueurs.get(capitaine)[2].nickname.substr(1));
        }
        await message.delete();
        if(user){
          await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("C'est "+user.username+" qui à été choisi").setDescription("Bravo pour ton élection, la médaille te reviens : soit digne de ce rôle !").setImage(lesRoles.get("Capitaine").get("custom")).setColor(couleurEmbedInfos));
          evenement("capitaine.svg", "Passation de pouvoir", "liserai-vert", "<b>Le capitaine | "+capitaine.username+"</b> étant décédé, il désigne <b>"+user.username+"</b> en tant que successeur"); // Pour les logs
          capitaine = user;
          if(lesJoueurs.get(capitaine)[2].hasPermission("ADMINISTRATOR")){
            lesJoueurs.get(capitaine)[2].send((new Discord.MessageEmbed().setTitle("Médaille").setDescription("Nous vous décernons cette médaille de Capitaine").setImage(lesRoles.get("Capitaine").get("custom")).setColor(couleurEmbedInfos).setFooter("Tu as trop de droits, je ne peux pas te la passer autour du cou malheureusement !")));
          }else if(lesJoueurs.get(capitaine)[2].nickname){
            lesJoueurs.get(capitaine)[2].setNickname("🎖️"+lesJoueurs.get(capitaine)[2].nickname);
          }else{
            lesJoueurs.get(capitaine)[2].setNickname("🎖️"+lesJoueurs.get(capitaine)[2].user.username);
          }
        }else{
          await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Aucun successeur n'a été désigné").setDescription("Malheureusement, "+capitaine.username+" est décédé(e) avant d'avoir pu nommer un successeur.\nLe village à donc décider de ne pas attribuer ce rôle à un autre joueur pour le moment").setColor(couleurEmbedInfos));
          evenement("capitaine.svg", "Passation de pouvoir", "liserai-rouge", "<b>Le capitaine | "+capitaine.username+"</b> est décédé(e) mais ne désigne pas de successeur"); // Pour les logs
          capitaine = undefined;
        }
        resolve();
      });
    });
  });
}

//==============================================================================
// CUPIDON
//==============================================================================

/*
UTILITÉ
Jouer le tour de Cupidon appelé la première nuit du jeu
*/
async function tourCupidon(){
  await channelTextuelVillage.send("```Cupidon se réveille !```");

  let [textEmbed, lesMembres] = getListeVillageAvecLettres();

  await channelTextuelCupidon.send(new Discord.MessageEmbed().setTitle("Vote pour les amoureux").setDescription(textEmbed).addField("Temps restant", tempsChoixCupidon+"s").setFooter("Vote pour les deux personnes à lier par l'amour").setColor(couleurEmbedSondage)).then(await function(message){
    return new Promise(async function(resolve){

      let listeVotesPossibles = [];
      for (let i = 0; i < lesJoueurs.size; i++) {
        await message.react(lesLettres.get(i));
        listeVotesPossibles.push(lesLettres.get(i));
      }

      let cpt = 0;
      let timer = setInterval(async function(){
        cpt ++;
        await message.edit(new Discord.MessageEmbed().setTitle("Vote pour les amoureux").setDescription(textEmbed).addField("Temps restant", (tempsChoixCupidon-cpt)+"s").setFooter("Vote pour les deux personnes à lier par l'amour").setColor(couleurEmbedSondage));
      }, 1000);

      const filter = (reaction, user) => listeVotesPossibles.includes(reaction.emoji.name) && !user.bot; // On s'assure que l'emoji soit bien un proposé par le bot et que ce ne soit pas la réaction du bot
      const collector = message.createReactionCollector(filter, { time: tempsChoixCupidon*1000 });

      collector.on('end', async function(collected){
        clearInterval(timer);
        if(collected.size < 2){
          await message.delete();
          await channelTextuelCupidon.send(new Discord.MessageEmbed().setTitle("Vote pour les amoureux").setDescription("Vous n'avez pas choisi deux amoureux parmis les propositions : tampis !").setFooter("Il n'y aura donc aucun amoureux dans cette partie").setColor(couleurEmbedError));

          evenementBulle("amoureux.svg", "<b>Cupidon</b> joue mais ne désigne pas d'amoureux"); // Pour les logs
        }else{
          let choix1, choix2; // Les deux amoureux

          if(collected.size == 2){
            choix1 = lesMembres[listeVotesPossibles.indexOf(collected.firstKey())]; // Le User correspondant à la lettre choisie
            choix2 = lesMembres[listeVotesPossibles.indexOf(collected.lastKey())]; // Le User correspondant à la lettre choisie
          }else{ // collected.size > 2
            choix1 = lesMembres[listeVotesPossibles.indexOf(collected.randomKey())]; // Le User choisi au hasard parmis les lettres votées
            choix2 = lesMembres[listeVotesPossibles.indexOf(collected.randomKey())]; // Le User choisi au hasard parmis les lettres votées
            while (choix1 == choix2) {
              choix2 = lesMembres[listeVotesPossibles.indexOf(collected.randomKey())]; // Le User est RE-choisi au hasard parmis les lettres votées
            }
          }

          evenementBulle("amoureux.svg", "<b>Cupidon</b> joue et désigne deux amoureux"); // Pour les logs
          evenementNonLineaire([["chat.svg", "<b>"+choix1.username +"</b> est notifié(e)"], ["chat.svg", "<b>"+choix2.username +"</b> est notifié(e)"]]); // Pour les logs

          await channelTextuelCupidon.send(new Discord.MessageEmbed().setTitle("Vote pour les amoureux").setDescription("Vous avez choisi de lier **"+choix1.username+"** et **"+choix2.username+"**").setFooter("Ils recevront un message pour être informés de leur attirance soudaine").setColor(couleurEmbedInfos));
          await channelTextuelVillage.send("```Les amoureux se réveillent et reçoivent un message de leur amant sur leur smartphone, en prennent connaissance et se rendorment !```");
          await lesJoueurs.get(choix1)[2].send("```Coucou chéri(e),\n\nJe te laisse ce petit message nocturne pour te dire que je t'aime à la vie à la mort !\n\n" + choix2.username+"```");
          await lesJoueurs.get(choix2)[2].send("```Coucou chéri(e),\n\nJe te laisse ce petit message nocturne pour te dire que je t'aime à la vie à la mort !\n\n" + choix1.username+"```");

          lesAmoureux = [choix1, choix2];
        }

        await channelTextuelVillage.send("```Cupidon se rendort```");
        resolve();
      });
    });
  });
}

//==============================================================================
// MEDIUM
//==============================================================================

/*
UTILITÉ
Choisi un pseudo au hasard pour tous les joeurs morts de sorte à les anonymiser dans le cas de l'envoie de message au medium
*/
function attribuerNomMort(){
  let numEsprit = [];
  for (let [user, liste] of lesMorts) {
    let randomNum = Math.floor(Math.random() * (lesMorts.size)) + 1;
    while (numEsprit.includes(randomNum)) {
      randomNum = Math.floor(Math.random() * (lesMorts.size)) + 1;
    }
    numEsprit.push();
    lesMorts.set(user, ['esprit'+randomNum, liste[1], liste[2]]);
  }

}

/*
UTILITÉ
Les actions du médium lors du commencement de la nuit
*/
function tourMediumDebutNuit(){
  attribuerNomMort();
  changerPermissionMediumMort(true);
  const filter = m => m.content != "" && ! m.author.bot;
  collectorMedium = channelTextuelMediumMort.createMessageCollector(filter); // Débuter le collecteur
  collectorMedium.on('collect', m => {
    channelTextuelMedium.send(lesMorts.get(m.author)[0] + ' : ' + m.content);
  });
}

/*
UTILITÉ
Les actions du médium lors de la fin de la nuit
*/
function tourMediumFinNuit(){
  changerPermissionMediumMort(false);
  collectorMedium.stop("fin nuit"); // Stopper le collecteur
  collectorMedium.on("stop", function(collected){
    separateur(); // Pour les logs
    evenementBulle("medium.png", "<b>Le Médium</b> a reçu "+collected.size+" messages"); // Pour les logs
  });
}

//==============================================================================
// TOUS LES RÔLES
//==============================================================================

/*
PARAMÈTRE
roleparticulier: le role au format string à expliquer
channelParticulier: Un channel spécifique dans lequel envoyer l'info -> utile pour l'envoyer à une seule personne qui demanderais des infos. Valeur par default sur le salon textuel du village pour donner à tous les règles
UTILITÉ
Creation d'un channel textuel pour un role du loup-garou partagé à tous les joueurs ayant ce rôle
*/
function expliquerRole(roleparticulier, channelParticulier){
  return new Promise(async function(resolve){
    switch(roleparticulier){
      case 'Loup-garou':
        await channelParticulier.send(new Discord.MessageEmbed().setTitle("Loups-Garous").setColor(couleurEmbedInfos).setThumbnail(lesRoles.get('Loup-garou').get('custom'))
        .setDescription("Chaque nuit, **ils dévorent** un Villageois. Le jour, ils essaient de **masquer leur identité nocturne** pour échapper à la vindicte populaire. Ils sont 1 à 6 suivant le nombre de joueurs.")
        .addField("Specificité", "En aucun cas un Loup-Garou ne peut dévorer un autre Loup-Garou."));
        break;
      case 'Villageois':
        await channelParticulier.send(new Discord.MessageEmbed().setTitle("Villageois").setColor(couleurEmbedInfos).setThumbnail(lesRoles.get('Villageois').get('custom'))
        .setDescription("Ses seules armes sont la **capacité d’analyse** des comportements pour identifier les Loups-Garous, et la **force de conviction** pour empêcher l’exécution de l’innocent qu’il est.")
        .addField("Specificité", "Il n’a aucune compétence particulière."));
        break;
      case 'Petite-fille':
        await channelParticulier.send(new Discord.MessageEmbed().setTitle("La Petite-Fille").setColor(couleurEmbedInfos).setThumbnail(lesRoles.get('Petite-fille').get('custom'))
        .setDescription("La Petite-Fille peut **espionner les Loups-Garous (uniquement) pendant leur réveil**.")
        .addField("Specificité", "Pour les besoins de notre adaptation, la Petite-fille a accès à un salon textuel supplémentaire qui lui permettra, lors de la séquence des Loups-Garous, d'observer les discussions de ces derniers **sans connaitre leur identité**.\n Soyez attentif, une rédaction particulière pourrait trahir son auteur :wink:."));
        break;
      case 'Chasseur':
        await channelParticulier.send(new Discord.MessageEmbed().setTitle("Chasseur").setColor(couleurEmbedInfos).setThumbnail(lesRoles.get('Chasseur').get('custom'))
        .setDescription("S’il se fait dévorer par les Loups-Garous ou exécuter malencontreusement par les joueurs, le Chasseur **doit répliquer** avant de rendre l’âme, en éliminant immédiatement n’importe quel autre joueur de son choix.")
        .addField("Specificité", "Il a un fusil :gun:."));
        break;
      case 'Cupidon':
        await channelParticulier.send(new Discord.MessageEmbed().setTitle("Cupidon").setColor(couleurEmbedInfos).setThumbnail(lesRoles.get('Cupidon').get('custom'))
        .setDescription("En décochant ses célèbres flèches magiques, Cupidon a le pouvoir de rendre **2 personnes amoureuses à jamais**.")
        .addField("La première nuit", "Il **désigne** les 2 joueurs amoureux. Cupidon peut, s’il le veut, se désigner comme l’un des deux amoureux.")
        .addField("Specificité", "Si l’un des amoureux est éliminé, **l’autre meurt de chagrin** immédiatement. Un amoureux ne doit jamais voter contre son aimé, ni lui porter aucun préjudice **(même pour faire semblant !)**.")
        .addField("Attention", "Attention : si l’un des deux amoureux est un **Loup-Garou** et l’autre un **Villageois**, le but de la partie change pour eux. Pour vivre en paix leur amour et gagner la partie, ils doivent éliminer tous les autres joueurs, Loups-Garous et Villageois, en respectant les règles de jeu."));
        break;
      case 'Sorciere':
        await channelParticulier.send(new Discord.MessageEmbed().setTitle("Sorcière").setColor(couleurEmbedInfos).setThumbnail(lesRoles.get('Sorciere').get('custom'))
        .setDescription("Elle sait concocter 2 potions extrêmement puissantes :\n• Une potion de guérison, pour ressusciter le joueur tué par les Loups-Garous\n• Une potion d’empoisonnement, utilisée la nuit pour éliminer un joueur")
        .addField("Attention", "Elle ne peut utiliser chaque potion **qu'une seule fois dans la partie**. Elle est limitée à l'utilisation d'une seule potion par nuit.")
        .addField("Specificité", "La Sorcière peut utiliser les potions à son profit, et donc se guérir elle-même si elle vient d’être attaquée par les Loups-Garous ou s'auto-empoisonner."));
      break;
      case 'Voyante':
        await channelParticulier.send(new Discord.MessageEmbed().setTitle("Voyante").setColor(couleurEmbedInfos).setThumbnail(lesRoles.get('Voyante').get('custom'))
        .setDescription("Chaque nuit, **elle découvre la vraie personnalité** d’un joueur de son choix. Elle doit aider les autres Villageois, mais rester discrète pour ne pas être démasquée par les Loups-Garous.")
        .addField("Specificité", "Elle aime les licornes :unicorn:."));
        break;
      case 'Capitaine':
        await channelParticulier.send(new Discord.MessageEmbed().setTitle("Capitaine").setColor(couleurEmbedInfos).setThumbnail(lesRoles.get('Capitaine').get('custom'))
        .setDescription("Cette carte est confiée à un des joueurs, en plus de sa carte personnage. Le Capitaine est élu(e) **par vote à la majorité**. On **ne peut refuser l’honneur** d’être Capitaine.")
        .addField("Specificité", "Dorénavant, les votes de ce joueur comptent pour 2 voix. Si il se fait éliminer, dans son dernier souffle il désigne son successeur."));
        break;
      case 'Medium':
        await channelParticulier.send(new Discord.MessageEmbed().setTitle("Medium").setColor(couleurEmbedInfos).setThumbnail(lesRoles.get('Medium').get('custom'))
        .setDescription("Le(a) Médium peut **entendre les morts**. Chaque nuit, les joueurs décédés pourront parler avec lui(elle) **sans réponse de sa part**")
        .addField("Specificité", "Une petite partie de ouija ?"));
        break;
      default:
        await channelParticulier.send(new Discord.MessageEmbed().setTitle("Rôle non reconnu").setDescription("Je ne reconnais pas le rôle nommé : \n**"+roleparticulier+"**").setFooter("Prévenez le modérateur si cela vous semble anormal :thumbup:").setColor(couleurEmbedError).setThumbnail("http://icons.iconarchive.com/icons/paomedia/small-n-flat/256/sign-error-icon.png"));
    }
    resolve();
  });
}
