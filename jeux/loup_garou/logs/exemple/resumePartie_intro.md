<!-- Page de présentation -->

<section>
  <center>

  Loup-Garou discord bot
  =======
  Résumé de la partie
  </center>

  <div class="space1"></div>

  <center>
    <img style="height: 10cm" alt="logo du jeu" src="./jeux/loup_garou/images_roles/custom/Loup-garou.png"/>

  _Design des cartes : DENÉCHÈRE Noémie_
  </center>

  <div class="space1"></div>

  #### Pour information
  Ce document est généré automatiquement à partir de données receuillies durant la partie. La mise en page de celui-ci peut donc être assez approximative par endroit. Son objectif est purement informatif.

  #### Auteurs
  + VERKERKE Xavier
  + DENÉCHÈRE Killian

</section>
