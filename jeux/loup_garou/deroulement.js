//==============================================================================
// Toutes les étapes de la partie
//==============================================================================


//==============================================================================
// PRÉPARATION PARTIE
//==============================================================================

/*
UTILITÉ
Attribuer un rôle à chaque personne voulant participer au loup-garou
PARAMÈTRE
listePersonnes -> La liste des personnes à qui il faut attribuer un rôle
*/
async function choixRoleRandom(listePersonnes){
  let chaine = "";
  let listeRolesNonSpeciaux = []; // Nécéssite un traitement particulier
  let listeRolesSpeciaux = []; // Un role qui peut-être attibué en début de partie sans traitement particulier
  let nbLoups, nbVillageois = [0, 0];

  for(let [role, map] of lesRoles){
    map.special ? listeRolesSpeciaux.push(role) : listeRolesNonSpeciaux.push(role);
  }

  switch(listePersonnes.length){
    case 1:
      nbLoups = 1;
      break;
    case 2:
    case 3:
    case 4:
      nbLoups = 1;
      nbVillageois = 1;
      break;
    case 5:
      nbLoups = 2;
      nbVillageois = 1;
      break;
    case 6:
    case 7:
      nbLoups = 2;
      nbVillageois = 2;
      break;
    case 8:
    case 9:
      nbLoups = 3;
      nbVillageois = 2;
      break;
    case 10:
      nbLoups = 3;
      nbVillageois = 3;
      break;
    case listePersonnes.length > 10:
      nbLoups = 4;
      nbVillageois = 3;
      console.log("Attention le nombre de joueur est supérieur à 10 : "+listePersonnes.length); // Penser à rajouter des roles
      break;
  }

  // Choix des rôles
  while(listePersonnes.length > 0){
    let personne = bot.randomItemFromListe(listePersonnes);
    let role;
    chaine += "- "+personne.username + "\n";

    if(nbLoups > 0){
      role = "Loup-garou";
      listeLoups.push(personne);
      nbLoups --;
      infoPartie.Role.loupGarou.nb ++;
      infoPartie.Role.loupGarou.present = 1; // Le nombre est mis à 1 car il équivaut à la valeur boolean true
    }else if (nbVillageois > 0) {
      role = "Villageois";
      nbVillageois --;
      infoPartie.Role.villageois.nb ++;
      infoPartie.Role.villageois.present = 1; // Le nombre est mis à 1 car il équivaut à la valeur boolean true
    }else if(listeRolesNonSpeciaux.length > 0){
      role = bot.randomItemFromListe(listeRolesNonSpeciaux); // Role choisi au hasard parmis les roles non spéciaux
      switch (role){
        case "Petite-fille":
          petiteFille = personne;
          infoPartie.Role.petiteFille.nb = 1; // Le nombre est mis à 1 car le rôle n'est sensé apparaitre qu'une fois
          infoPartie.Role.petiteFille.present = 1; // Le nombre est mis à 1 car il équivaut à la valeur boolean true
          break;
        case "Chasseur":
          chasseur = personne;
          infoPartie.Role.chasseur.nb = 1; // Le nombre est mis à 1 car le rôle n'est sensé apparaitre qu'une fois
          infoPartie.Role.chasseur.present = 1; // Le nombre est mis à 1 car il équivaut à la valeur boolean true
          break;
        case "Cupidon":
          cupidon = personne;
          infoPartie.Role.cupidon.nb = 1; // Le nombre est mis à 1 car le rôle n'est sensé apparaitre qu'une fois
          infoPartie.Role.cupidon.present = 1; // Le nombre est mis à 1 car il équivaut à la valeur boolean true
          break;
        case "Sorciere":
          sorciere = personne;
          infoPartie.Role.sorciere.nb = 1; // Le nombre est mis à 1 car le rôle n'est sensé apparaitre qu'une fois
          infoPartie.Role.sorciere.present = 1; // Le nombre est mis à 1 car il équivaut à la valeur boolean true
          break;
        case "Voyante":
          voyante = personne;
          infoPartie.Role.voyante.nb = 1; // Le nombre est mis à 1 car le rôle n'est sensé apparaitre qu'une fois
          infoPartie.Role.voyante.present = 1; // Le nombre est mis à 1 car il équivaut à la valeur boolean true
          break;
        case "Medium":
          medium = personne;
          infoPartie.Role.medium.nb = 1; // Le nombre est mis à 1 car le rôle n'est sensé apparaitre qu'une fois
          infoPartie.Role.medium.present = 1; // Le nombre est mis à 1 car il équivaut à la valeur boolean true
          break;
        default:
          console.log("ChoixRoleRandom : le role attribué n'est pas prévu dans le switch -> "+role);
          infoPartie.Role.autre.nb ++;
          infoPartie.Role.autre.present = 1;
      }
      listeRolesNonSpeciaux.splice(listeRolesNonSpeciaux.indexOf(role), 1); // Enlever le rôle de la liste des rôles non spéciaux
    }else{
      role = "Il n'y a plus de rôles en réserve -> préviens l'administrateur du bot !";
    }
    lesJoueurs.set(personne, [role]);
    listePersonnes.splice(listePersonnes.indexOf(personne), 1); // On retire la personne de la liste des pers à qui il faut assigner un rôle
  }

  bot.fs.writeFile("./jeux/loup_garou/tableau/infoPartie.json", JSON.stringify(infoPartie, null, 4), async function(err){ //on écrit dans le fichier 'infoPartie.json' les informations précédentes
    if(err) throw err;
    await genererTableauRoles();

    for(let [pers, listeRoles] of lesJoueurs){
      await expliquerRole(listeRoles[0], pers);
    }

    guildeCourante.channels.create('🐺 Partie LG 🐺', {type: 'category'}).then(channel => { // Création de la catégorie des channels destinés au jeu
      categoryChannelLG = channel;
    });

    attribuerRoles(chaine);
  });
}

/*
UTILITÉ
Mettre les rôles sur discord en adéquation avec le choix aléatoire de la fonction précédente
Les rôles créés ont tous le même nom pour qu'on ne puisse pas les différencier mais aurons des accès différents aux channels
PARAMÈTRE
chaine -> Le string contenant les participants
*/
async function attribuerRoles(chaine){
  let i = 1;
  let stringRole = new Map(); // Dictionnaire avec (Role)role -> (String)role
  for(let [pers, listeRoles] of lesJoueurs){
    let member = guildeCourante.members.cache.get(pers.id);
    await guildeCourante.roles.create({data: { name: 'Joueur'+i, color: 0, },reason: 'Un role pour la partie en cours'}).then(function(resRole){
      stringRole.set(resRole, listeRoles[0]);
      listeRoles.push(resRole);
      listeRoles.push(member);
      member.roles.add(resRole); // Donner le role à la personne
      fonctionStop.get("roles").push(resRole); // Ajout du role pour la supression
    });
    i++;
  }

  pageJoueurs(1); // Pour les logs
  await creerSalons(stringRole);
  commencerHistoire(chaine);
}

/*
UTILITÉ
Creation d'un channel textuel pour un role du loup-garou partagé à tous les joueurs ayant ce rôle
PARAMÈTRE
stringRole -> Dictionnaire avec (Role)role -> (String)role à transmettre au changements de permission
*/
function creerSalons(stringRole){
  return new Promise(async function(resolve) {
    // Village
    await guildeCourante.channels.create('village-textuel').then(async function(channel){ // Création du channel textuel pour le village
      channel.setParent(categoryChannelLG); // Définition du parent de ce channel
      channelTextuelVillage = channel;
      await changerPermissionVillage(channelTextuelVillage, stringRole); // Change les permissions d'accès au channel textuel du village
      fonctionStop.get("salons").push(channel); // Pour la supression
    });

    await guildeCourante.channels.create('village-vocal', {type: 'voice'}).then(async function(channel){ // Création du channel vocal pour le village
      channel.setParent(categoryChannelLG); // Définition du parent de ce channel
      channelVocalVillage = channel;
      await changerPermissionVillage(channelVocalVillage, stringRole); // Change les permissions d'accès au channel vocal du village
      bot.on('voiceStateUpdate', listenerVoiceUpdateJour);// On met le listener du vocal du jour
      fonctionStop.get("salons").push(channel); // Pour la supression
    });

    // Autres
    await guildeCourante.channels.create('loup-garou').then(async function(channel) { // Création du channel textuel pour les loups-garou
      channel.setParent(categoryChannelLG); // Définition du parent de ce channel
      channelTextuelLoupsGarous = channel;
      changerPermission(channel, 'Loup-garou', stringRole);
      channel.send(new Discord.MessageEmbed().setTitle("Bienvenue braves Loups").setDescription("Vous pourrez discuter avec vos congénaires uniquement en période nocturne.").setFooter("Soyez bon joueur et ne discutez pas en messages privés, merci 😗").setColor(couleurEmbedExplications));
      fonctionStop.get("salons").push(channel); // Pour la supression
    });

    await guildeCourante.channels.create('morts').then(async function(channel) { // Création du channel textuel pour les morts
      channel.setParent(categoryChannelLG); // Définition du parent de ce channel
      channelMort = channel;
      changerPermission(channelMort, "aucun", stringRole); // Change les permissions d'accès à personne car c'est le début et qu'il n'y a pas encore de morts
      channel.send(new Discord.MessageEmbed().setTitle("Bienvenue dans le monde des morts").setDescription("Malheureusement vous êtes apparemment décédé !\nPensez que si un medium joue, vous pouvez lui transmettre vos informations **sans jamais indiquer votre ancien role ou pseudo**").setFooter("Vote injuste ? Jambon pour les Loups ? Quelle que soit la raison, nous vous accueillons ici avec plaisir le temps de la fin de la partie.").setColor(couleurEmbedExplications));
      fonctionStop.get("salons").push(channel); // Pour la supression
    });

    if(petiteFille){
      await guildeCourante.channels.create('petite-fille').then(async function(channel) { // Création du channel textuel pour la petite fille
        channel.setParent(categoryChannelLG); // Défnition du parent de ce channel
        channelTextuelPetiteFille = channel;
        changerPermission(channel, 'Petite-fille', stringRole);
        channel.send(new Discord.MessageEmbed().setTitle("Bienvenue brave Petite-fille").setDescription("Vous ne pouvez pas écrire dans ce channel mais n'ayez crainte, vous pourrez agir en temps voulu !").setFooter("Vous recevrez un message pour vous indiquer lorsque vous pourrez épiez ces montres que l'on apelle 'Loups-Garous'").setColor(couleurEmbedExplications));
        fonctionStop.get("salons").push(channel); // Pour la supression
      });
    }

    if(cupidon){
      await guildeCourante.channels.create('cupidon').then(async function(channel){ // Création du channel textuel pour cupidon
        channel.setParent(categoryChannelLG);
        channelTextuelCupidon = channel;
        changerPermission(channel, 'Cupidon', stringRole);
        channel.send(new Discord.MessageEmbed().setTitle("Bienvenue brave Cupidon").setDescription("Tu pourra effectuer ton rôle dans ce channel puis il disparaitra").setFooter("Tu recevra un message pour faire ton vote").setColor(couleurEmbedExplications));
        fonctionStop.get("salons").push(channel);
      });
    }

    if(voyante){
      await guildeCourante.channels.create('voyante').then(async function(channel){ // Création du channel textuel pour la voyante
        channel.setParent(categoryChannelLG);
        channelTextuelVoyante = channel;
        changerPermission(channel, 'Voyante', stringRole);
        channel.send(new Discord.MessageEmbed().setTitle("Bienvenue brave Voyante").setDescription("Chaque nuit, tu recevra ici un message te permettant de choisir une personne").setFooter("Attention à ne pas choisir deux fois la même personne 😆").setColor(couleurEmbedExplications));
        fonctionStop.get("salons").push(channel);
      });
    }

    if(sorciere){
      await guildeCourante.channels.create('sorcière').then(async function(channel){ // Création du channel textuel pour la sorcière
        channel.setParent(categoryChannelLG);
        channelTextuelSorciere = channel;
        changerPermission(channel, 'Sorciere', stringRole);
        channel.send(new Discord.MessageEmbed().setTitle("Bienvenue brave Sorcière").setDescription("Tant que toutes tes potions ne seront pas utilisées, tu pourra effectuer tes choix ici").setColor(couleurEmbedExplications));
        fonctionStop.get("salons").push(channel);
      });
    }

    if(medium){
      await guildeCourante.channels.create('médium').then(async function(channel){ // Création du channel textuel pour le medium
        channel.setParent(categoryChannelLG);
        channelTextuelMedium = channel;
        changerPermission(channel, 'Medium', stringRole);
        channel.send(new Discord.MessageEmbed().setTitle("Bienvenue brave Médium").setDescription("Chaque nuit les morts pourront te contacter pour te donner les informations dont ils disposent. Tu ne peux pas leur répondre et leurs messages seront anonymisés.").setFooter("Attention : esprit1 ne correspont pas forcément au premier mort de la partie et les noms des esprits sont réinitialisés à chaque nuit.").setColor(couleurEmbedExplications));
        fonctionStop.get("salons").push(channel);
      });

      await guildeCourante.channels.create('médium-mort').then(async function(channel){ // Création du channel textuel pour les morts vouant parler au medium
        channel.setParent(categoryChannelLG);
        channelTextuelMediumMort = channel;
        changerPermission(channel, 'aucun', stringRole);
        changerPermissionMediumMort(false);
        channel.send(new Discord.MessageEmbed().setTitle("Bienvenue humble Mort").setDescription("Chaque nuit tu pourras communiquer au médium les informations dont tu disposent. Tu auras un petit délai entre chaque message et tes messages seront anonymisés.").setFooter("Il n'est pas interdit de dire ton pseudo ni ton rôle car il est possible que tu mentes au médium, fait passer tes idées pour gagner la partie").setColor(couleurEmbedExplications));
        fonctionStop.get("salons").push(channel);
      });
    }

    resolve();
  });
}

//==============================================================================
// INTERIEUR PARTIE
//==============================================================================

/*
UTILITÉ
Informer du début de la partie dans X temps et lancer le premier message
PARAMÈTRE
chaine -> La liste des personens participants sous forme de string
*/
function commencerHistoire(chaine){
  channelPrincipal.send(new Discord.MessageEmbed().setTitle("Lancement de la partie").setDescription("**Participants**\n"+chaine+"\nConsultez le tableau des rôles de la partie n'importe quand ici\n"+lienSite+"/joueurs.html").addField("Temps restant avant le lancement", tempsAttenteLancementPartie+"s").setFooter("Vous allez recevoir vos rôles par message privé (soyez discrets :zipper_mouth:)\nRegardez bien les salons auxquels vous avez maintenant accès").setColor(couleurEmbedInfos)).then(function(message){
    fonctionStop.get('messages').push(message);
    let cpt = 0;
    let timer = setInterval(function() {
      cpt ++;
      message.edit(new Discord.MessageEmbed().setTitle("Lancement de la partie").setDescription("**Participants**\n"+chaine+"\nConsultez le tableau des rôles de la partie n'importe quand ici\n"+lienSite+"/joueurs.html").addField("Temps restant avant le lancement", (tempsAttenteLancementPartie-cpt)+"s").setFooter("Vous allez recevoir vos rôles par message privé\nRegardez bien les salons auxquels vous avez maintenant accès").setColor(couleurEmbedInfos));
      if(cpt == tempsAttenteLancementPartie){
        clearInterval(timer); // On arrete le décompte
        channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Sondage").setDescription("Voulez-vous un rappel des règles ?").addField("Temps restant", tempsAttenteLancementPartie+"s").setColor(couleurEmbedSondage)).then(async function(sondage){
          reactSet = false;
          await sondage.react("✅");
          await sondage.react("⛔").then(function(){reactSet = true;});
          cpt = 0;
          let timer2 = setInterval(async function(){
            cpt ++;
            sondage.edit(new Discord.MessageEmbed().setTitle("Sondage").setDescription("Voulez-vous un rappel des règles ?").addField("Temps restant", (tempsAttenteAvantFinSondage-cpt)+"s").setColor(couleurEmbedSondage));
            if(cpt == tempsAttenteAvantFinSondage){
              if(sondage.reactions.cache.get("✅").count > 1){
                await expliquerRegles();
                channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Sondage").setDescription("Avez-vous tous pris connaissance des règles ?").setFooter("Il faut que tout les participants votent ✅ pour lancer la première nuit").setColor(couleurEmbedSondage)).then(function(newMessage){
                  newMessage.react("✅");
                  let timer3 = setInterval(function(){
                    if(newMessage.reactions.cache.get("✅").count == lesJoueurs.size+1){
                      clearInterval(timer3);
                      for(let [id, message] of newMessage.channel.messages.cache){
                        message.delete();
                      }
                      lancerPremiereNuit();
                    }
                  }, 1000);
                });
              }else{
                lancerPremiereNuit();
              }
              sondage.delete();
              clearInterval(timer2);
            }
            if (reactSet && sondage.reactions.cache.get("⛔").count == lesJoueurs.size + 1) {
              cpt = tempsAttenteAvantFinSondage - 1;
            }
          }, 1000);
        });
      }
    }, 1000);
  });
}

/*
UTILITÉ
Mettre la période de nuit (mute, bruit du bot, histoire etc ...)
*/
async function lancerPremiereNuit(){
  nouvellePage("Déroulé de la partie"); // Pour les logs : On démarre une nouvelle page

  debutNuit();
  separateur(); // Pour les logs

  // Appel à Cupidon
  if(cupidon){
    tourCupidon();
  }else{
    evenementBulle("amoureux.svg", "<b>Cupidon</b> n'est pas présent, son tour passe"); // Pour les logs
    await channelTextuelVillage.send("```Cupidon n'est pas présent dans cette partie, son tour passe```");
  }

  setTimeout(async function(){
    if(channelTextuelCupidon){
      await channelTextuelCupidon.delete("Fin des actions du role");
    }
    lancerPremierJour();
  }, tempsAvantLancementJour*1000);
}

/*
UTILITÉ
Mettre le premier jour avec le vote du Capitaine
*/
async function lancerPremierJour(){
  separateur(); // Pour les logs
  debutJour();

  // Liste des joueurs pour le vote
  let [textEmbed, lesMembres] = getListeVillageAvecLettres();

  // Lancement du vote
  channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Votez pour le Capitaine").setDescription(textEmbed).addField("Temps restant", tempsElectionCapitaine+"s").setFooter("Vote pour celui qui saura vous guider vers la victoire. Pour rappel, l'élu ne pourra pas refuser.").setColor(couleurEmbedSondage)).then(async function(voteCapitaine){
    for(let j=0; j<lesJoueurs.size; j++){
      await voteCapitaine.react(lesLettres.get(j)); // On met une réaction pour chaque personne
    }

    let cpt = 0;
    let timer = setInterval(async function(){
      cpt ++;

      if(cpt == tempsElectionCapitaine){
        clearInterval(timer); // On arrete le décompte

        let gagnant = [];
        let max = 0;
        for (let j=0; j<lesJoueurs.size; j++) { // Parcour de tous les votes
          let nbVoix = voteCapitaine.reactions.cache.get(lesLettres.get(j)).count; // Pour la lettre courante
          if(nbVoix > 1){
            if(nbVoix > max){
              gagnant = [lesMembres[j]];
              max = nbVoix;
            }else if (nbVoix == max){
              gagnant.push(lesMembres[j]);
            }
          }
        }

        separateur(); // Pour les logs
        await voteCapitaine.delete();
        if(gagnant.length == 1){ // Si il y a une personne qui a gagné
          capitaine = gagnant[0];
          evenement("capitaine.svg", "Vote du Capitaine", "liserai-vert", "<b>"+capitaine.username+"</b> est élu(e) à la majorité"); // Pour les logs
          await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Vote clos !").setDescription("Les votes sont clos, on dirait bien que nous avons un gagnant : **" + capitaine.username + "**").setThumbnail(lesRoles.get("Capitaine").get("custom")).setFooter("Bon courage !").setColor(couleurEmbedInfos));
        }else if(gagnant.length>1){
          capitaine = bot.randomItemFromListe(gagnant);
          evenement("capitaine.svg", "Vote du Capitaine", "liserai-jaune", "Avec un nombre de vote équivalant pour plusieurs joueurs, c'est <b>"+capitaine.username+"</b> qui est finalement désigné"); // Pour les logs
          await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Impossible de vous mettre d'accord...").setDescription("Les votes sont clos, il y a eu égalité sur les votes, c'est donc M.le Hasard (huissier de justice) qui a décidé parmis les ex aequo, c'est : **" + capitaine.username + "** qui a gagné  la campagne.").setThumbnail(lesRoles.get("Capitaine").get("custom")).setFooter("Faudrat ce plaindre auprès de M.le Hasard si vous n'êtes pas content").setColor(couleurEmbedInfos));
        }else{
          capitaine = bot.randomItemFromListe(lesMembres);
          evenement("capitaine.svg", "Vote du Capitaine", "liserai-rouge", "Personne n'as voté, <b>"+capitaine.username+"</b> est élu(e) au hasard"); // Pour les logs
          await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Personne n'a donné son avis !").setDescription("Vu que vous savez pas donner votre avis, le choix sera donc fait aléatoirement. L'heureux élu est : **" + capitaine.username + "**").setThumbnail(lesRoles.get("Capitaine").get("custom")).setFooter("Faudrat pas venir se plaindre...").setColor(couleurEmbedError));
        }

        // Mise en place du pseudo
        if(lesJoueurs.get(capitaine)[2].hasPermission("ADMINISTRATOR")){
          lesJoueurs.get(capitaine)[2].send((new Discord.MessageEmbed().setTitle("Médaille").setDescription("Nous vous décernons cette médaille de Capitaine").setImage(lesRoles.get("Capitaine").get("custom")).setColor(couleurEmbedInfos).setFooter("Tu as trop de droits, je ne peux pas te la passer autour du cou malheureusement !")));
        }else if(lesJoueurs.get(capitaine)[2].nickname){
          lesJoueurs.get(capitaine)[2].setNickname("🎖️"+lesJoueurs.get(capitaine)[2].nickname);
        }else{
          lesJoueurs.get(capitaine)[2].setNickname("🎖️"+lesJoueurs.get(capitaine)[2].user.username);
        }
        setTimeout(function(){jouerNuit();}, tempsAvantLancementJour*1000);
      }else{
        voteCapitaine.edit(new Discord.MessageEmbed().setTitle("Votez pour le Capitaine").setDescription(textEmbed).addField("Temps restant", (tempsElectionCapitaine-cpt)+"s").setFooter("Vote pour celui qui saura vous guider vers la victoire. Pour rappel, l'élu ne pourra pas refuser.").setColor(couleurEmbedSondage));
      }
    }, 1000); // Éxécution toutes les secondes
  });
}

/*
UTILITÉ
Lance une nuit classique
*/
async function jouerNuit(){
  separateur(); // Pour les logs
  debutNuit();

  let collectorMedium; // Pour que le médium fasse ses actions

  if(medium){ // La seconde partie du role est plus bas dans la fonction
    tourMediumDebutNuit();
  }

  if(voyante){ // La voyante est présente
    separateur(); // Pour les logs
    await tourVoyante();
  }

  separateur(); // Pour les logs
  await tourLoupGarou(); // Active aussi le tour de la petite-fille
  evenementBulle("loup.png", "<b>Les Loups-Garous</b> discutent"); // Pour les logs
  if(! messagesTemporaires[1]){
    messagesTemporaires[1] = "<b>La Petite-Fille</b> n'épie pas les Loups cette nuit"; // Pour les logs
  }
  evenementNonLineaire([["loup.png", messagesTemporaires[0]], ["fille.svg", messagesTemporaires[1]]]); // Pour les logs
  messagesTemporaires = []; // Pour les logs

  if(sorciere){ // La sorcière est présente et il lui reste au moins un pouvoir à utiliser
    separateur(); // Pour les logs
    await tourSorciere();
  }

  if(medium){
    tourMediumFinNuit();
  }

  setTimeout(function(){jouerJour();}, tempsAvantLancementJour*1000);
}

/*
UTILITÉ
Lance une journée classique
*/
async function jouerJour(){
  separateur(); // Pour les logs
  debutJour();

  // Annonce des morts de la nuit
  await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Cette nuit, ||**"+mortsDeLaNuit.length+"** personne(s)|| est/sont morte(s)").setColor(couleurEmbedExplications));

  if(mortsDeLaNuit.length > 0){
    await verifierMortsDeLaNuit();
  }

  let etatVictoire = partieFinie();
  if(etatVictoire.startsWith('v')){ // Une condition de victoire a été remplie
    finDePartie(etatVictoire);
  }else{
    separateur(); // Pour les logs
    await setTimeout(async function(){
      await voteVillage();
      etatVictoire = partieFinie();
      if(etatVictoire.startsWith('v')){
        finDePartie(etatVictoire);
      }else{
        setTimeout(function(){
          jouerNuit();
        }, tempsAvantLancementJour*1000);
      }
    }, tempsAvantAnnonceRolePersonneElimine*1000);
  }
}

/*
UTILITÉ
Lance le vote au village pour désigner un coupable à tuer le jour
*/
function voteVillage(){
  return new Promise(function(resolve) {
    const [textEmbed, lesMembres] = getListeVillageAvecLettres();
    channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Votez pour le supposé Loup-Garou").setDescription("Vous vous retrouvez sur la place centrale du village pour voter pour la personne à éliminer. La tension est palpable... Les personnes contre qui vous pouvez voter sont : \n\n" + textEmbed).addField("Temps restant", tempsVoteElimination+"s").setFooter("Pour rappel, la personne avec le plus de voix meurt. S'il y a égalité, c'est le vote du Capitaine qui prime et si il y a égalité sans que le Capitaine ne vote alors M.le Hasard (huissier de justice) se chargera de désigner un coupable.").setColor(couleurEmbedSondage)).then(async function(message){

      let listeVotesPossibles = [];
      for(let i=0; i<lesMembres.length; i++){
        listeVotesPossibles.push(lesLettres.get(i));
      }

      let cpt = 0;
      let timer = setInterval(async function(){
        cpt ++;
        await message.edit(new Discord.MessageEmbed().setTitle("Votez pour le supposé Loup-Garou").setDescription("Vous vous retrouvez sur la place centrale du village pour voter pour la personne à éliminer. La tension est palpable... Les personnes contre qui vous pouvez voter sont : \n\n" + textEmbed).addField("Temps restant", (tempsVoteElimination - cpt)+"s").setFooter("Pour rappel, la personne avec le plus de voix meurt. S'il y a égalité, c'est le vote du Capitaine qui prime et si il y a égalité sans que le Capitaine ne vote alors M.le Hasard (huissier de justice) se chargera de désigner un coupable.").setColor(couleurEmbedSondage));
      }, 1000);

      const filter = (reaction, user) => listeVotesPossibles.includes(reaction.emoji.name) && !user.bot; // On s'assure que l'emoji soit bien un proposé par le bot et que ce ne soit pas la réaction du bot
      const collector = message.createReactionCollector(filter, { time: tempsVoteElimination*1000 });

      for(let i=0; i<lesMembres.length; i++){
        await message.react(lesLettres.get(i)); // On met les réactions après le collector pour prendre les réactions de tout le monde
      }


      collector.on('collect', async function(reaction, user){
        for (const reac of message.reactions.cache.filter(reaction => reaction.users.cache.has(user.id)).values()) {
          if(reac != reaction){
            await reac.users.remove(user.id);
          }
        }
      });

      collector.on('end', async function(collected, reason){
        clearInterval(timer);

        if(collected.size > 0){
          let votesMax = [];// Le ou les émojis ayant reçus le plus de votes
          let max = 0;
          for(const [emoji, messageReaction] of collected){
            let count = messageReaction.count;
            if(capitaine && messageReaction.users.cache.find(user => user == capitaine) != undefined){
              count ++;
            }
            if(votesMax.length == 0 || count == max){ // Si l'émoji est le premier (liste encore vide) ou que l'emoji courant possède le même nombre de vote que le max
              votesMax.push(emoji);
              max = count;
            }else if(count > max){ // Si l'emoji courant est voté en plus grand nombre que le ou les emoji max
              votesMax = [emoji];
              max = count;
            }
          }

          let elimine, desc;
          if(votesMax.length == 1){ // Une personne voté en majorité
            elimine = lesMembres[listeVotesPossibles.indexOf(votesMax[0])]; // Le User correspondant à la lettre choisie
            desc    = " les **vrais** villageois ont décidé de vous éliminer et leur sentance est irrévocable !";
          }else{ // Il y a plusieurs personnes ayant le même nombre de votes

            if(capitaine){ // Si il y a un capitaine, on vérifie pour qui il a voté
              let voteCapitaine; // Contient le vote du capitaine s'il a réagi et reste vide s'il n'a pas réagi pour ce vote
              for(const emoji of votesMax){
                if(collected.get(emoji).users.cache.find(user => user == capitaine) != undefined){
                  voteCapitaine = emoji;
                }
              }

              if(!voteCapitaine){ // Le capitaine n'a pas voté pour une des personnes qui ont la majorité des votes donc on prend au hasard parmis les ex aequo
                elimine = lesMembres[listeVotesPossibles.indexOf(bot.randomItemFromListe(votesMax))];
                desc    = "plusieurs personnes ont été voté ex aequo et le **capitaine** n'ayant voté pour personne, vous avez été désigné coupable par M.le Hasard";
              }else{ // Le capitaine a voté pour une des personnes qui ont reçu le plus de votes
                elimine = lesMembres[listeVotesPossibles.indexOf(voteCapitaine)];
                desc    = "plusieurs personnes ont été voté ex aequo et le **capitaine** ayant voté pour vous, vous avez été désigné coupable par M.le Hasard";
              }
            }else{ // Pas de capitaine donc on choisi au hasard
              elimine = lesMembres[listeVotesPossibles.indexOf(bot.randomItemFromListe(votesMax))]; // Le User correspondant à la lettre choisie
              desc    = "les **vrais** villageois ont décidé de vous éliminer et leur sentance est irrévocable !";
            }
          }

          await message.delete();
          evenement("vote.svg", "Le village vote", "liserai-vert", "<b>Le village</b> vote l'élimination de <b>"+elimine.username+"</b> ("+lesJoueurs.get(elimine)[0]+")"); // Pour les logs
          await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Vote clos !").setDescription("**" + elimine.username + "**, "+ desc).setFooter("Ce n'est qu'un aurevoir !").setColor(couleurEmbedInfos));
          await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle(":skull: "+elimine.username+" était... :skull:")).then(async function(msg){
            await supprJoueurMort(msg, elimine);
          });

        }else{
          await message.delete();
          evenement("vote.svg", "Le village vote", "liserai-rouge", "<b>Le village</b> n'a pas voté, personne n'est éliminé"); // Pour les logs
          await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Personne n'a donné son avis !").setDescription("Comme aucun des joueurs n'a donné son avis, il n'y aura pas de sacrifice aujourd'hui.").setColor(couleurEmbedError));
          setTimeout(async function(){ resolve(); }, tempsAvantAnnonceRolePersonneElimine*1000);
        }
      });
    });
  });
}

/*
PARAMÈTRE
etatVictoire: str annoncant le gagnant de la partie en cours
UTILITÉ
Annoncer la fin de la partie avec l'affichage des stats, du déroulé de la partie, des gaganants. réinitialise les varaibles de partie pour pouvoir en lancer une nouvelle
*/
async function finDePartie(etatVictoire){
  separateur(); // Pour les logs
  let listeGagnants = []; // Corresponds aux personnes ayants gagnés la partie, les joueurs encore en vie par exemple
  let positions;
  let calque1;
  let listeImagesFusion = [];

  switch(etatVictoire){
    case "vl": // Victoire des loups
      channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Et c'est une victoire des loups !").setDescription("Les morts vont revenir à la vie pour que vous puissiez vous battre comme il se doit :)"));
      evenement("trophe.png", "Victoire des loups", "victoire"); // Pour les logs
      for(let [user, liste] of lesJoueurs){
        listeGagnants.push(user);
      }
      listeImagesFusion.push("./jeux/loup_garou/logs/icons/fond.png");
      calque1 = "./jeux/loup_garou/logs/icons/gagnants_loups.png";
      positions = new Map([
        [0, new Map([["x", 377], ["y", 235], ["taille", 128]])],
        [1, new Map([["x", 1], ["y", 386], ["taille", 128]])],
        [2, new Map([["x", 467], ["y", 457], ["taille", 128]])],
        [3, new Map([["x", 11], ["y", 18], ["taille", 64 ]])],
        [4, new Map([["x", 221], ["y", 1], ["taille", 64 ]])],
        [5, new Map([["x", 541], ["y", 31], ["taille", 64 ]])],
        [6, new Map([["x", 529], ["y", 348], ["taille", 64 ]])],
        [7, new Map([["x", 59], ["y", 549], ["taille", 64 ]])]
      ]);
      break;

    case "vp":
      channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Et ce n'est pas une victoire !").setDescription("Les morts vont revenir à la vie pour que vous puissiez vous battre comme il se doit :)\nPS: Peut mieux faire !"));
      evenement("trophe.png", "Pas de victoire", "pasVictoire"); // Pour les logs
      break;

    case "vv": // Victoire village
      channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Et c'est une victoire du village !").setDescription("Les morts vont revenir à la vie pour que vous puissiez vous battre comme il se doit :)"));
      evenement("trophe.png", "Victoire du village", "victoire"); // Pour les logs
      for(let [user, liste] of lesJoueurs){
        listeGagnants.push(user);
      }
      listeImagesFusion.push("./jeux/loup_garou/logs/icons/fond.png");
      calque1 = "./jeux/loup_garou/logs/icons/gagnants_village.png";
      positions = new Map([
        [0, new Map([["x", 377], ["y", 235], ["taille", 128]])],
        [1, new Map([["x",1], ["y", 386], ["taille", 128]])],
        [2, new Map([["x", 467], ["y", 457], ["taille", 128]])],
        [3, new Map([["x", 11], ["y", 18], ["taille", 64 ]])],
        [4, new Map([["x", 360], ["y", 35], ["taille", 64 ]])],
        [5, new Map([["x", 541], ["y", 31], ["taille", 64 ]])],
        [6, new Map([["x", 529], ["y", 348], ["taille", 64 ]])],
        [7, new Map([["x", 59], ["y", 549], ["taille", 64 ]])]
      ]);
      break;

    case "va": // Victoire des amoureux
      channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Et c'est une victoire des amoureux !").setDescription("Les morts vont revenir à la vie pour que vous puissiez vous battre comme il se doit :)"));
      evenement("trophe.png", "Victoire des amoureux", "victoire"); // Pour les logs
      listeGagnants = lesAmoureux;
      listeImagesFusion.push("./jeux/loup_garou/logs/icons/fond_amoureux.png");
      calque1 = "./jeux/loup_garou/logs/icons/gagnants_amoureux.png";
      positions = new Map([
        [0, new Map([["x", 32], ["y", 176], ["taille", 256]])],
        [1, new Map([["x", 301], ["y", 212], ["taille", 256]])]
      ]);
      break;

    default:
      console.log("Victoire non reconnu : "+etatVictoire);
      return ;
  }

  // On unmute tous les joueurs (y compris les morts)
  for(let pers of channelVocalVillage.members){
    if( !pers[1].user.bot){ // Ne pas toucher au bot
      pers[1].voice.setMute(false);
    }
  }

  if(listeGagnants.length > 0){
    for(let i=0; i<listeGagnants.length; i++){
      if(i>=7){
        break;
      }
      listeImagesFusion.push({src: listeGagnants[i].avatarURL({format: "png", size: positions.get(i).get("taille")}), x: positions.get(i).get("x") , y: positions.get(i).get("y")});
    }

    listeImagesFusion.push(calque1); // On le rajoute à la fin car il fait cache pour les images rectangles (qu'elles apparaissent en rond)

    bot.mergeImages(listeImagesFusion, {Canvas: Canvas, Image: Image}).then(function(res){
      res = res.split(';base64,').pop();
      bot.fs.writeFile('./jeux/loup_garou/logs/temporaire/gagnants.png', res, {encoding: 'base64'}, async function(err){
        if(err) throw err;
          notationPartie(etatVictoire);
      });
    });
  }else{ // Si égalité
    notationPartie(etatVictoire);
  }
}

//==============================================================================
// RESSOURCES PARTIE
//==============================================================================

/*
UTILITÉ
Ce listeer activité uniquement pendant les périodes de nuit du jeu permet de mute les joueurs rejoignant la partie pendant cette période et de les unmutes s'ils quittent le vocal alors qu'ils sont mute
En réalité, il est activé lors de jin/leave et mute/unmute sur channel vocal -> c'est la raison de la condition avec mute
PARAMÈTRES
oldVoiceState -> (Voice state) L'état vocal du guildMemeber qui change/se déconnecte de channel vocal ou se mute/unmute avant le changement en question
newVoiceState -> (Voice state) L'état vocal du guildMemeber qui change/se déconnecte de channel vocal ou se mute/unmute après le changement en question
*/
function listenerVoiceUpdateNuit(oldVoiceState, newVoiceState){
  if(partieEnCours){ // Pour éviter l'envoie du message lors de la supression des channels en fin de partie
    const newVoiceStateChannel = newVoiceState.channel;
    const oldVoiceStateChannel = oldVoiceState.channel;

    if((newVoiceStateChannel != oldVoiceStateChannel) && !newVoiceState.member.user.bot){ // L'event n'est pas lancé pour une raison de mute/unmute mais bel et bien pour un changement de channel et ce n'est pas du bot que l'on parle
      if(newVoiceStateChannel == channelVocalVillage){ // Il a rejoint un channel vocal et c'est le channel vocal du village
        newVoiceState.member.voice.setMute(true);
      }else if(oldVoiceStateChannel == channelVocalVillage && newVoiceStateChannel != undefined){ // Il change de channel vocal à partir du voc village
        newVoiceState.member.voice.setMute(false);
      }else if(oldVoiceStateChannel == channelVocalVillage && newVoiceStateChannel == undefined){ // Il s'est déconnecté, on ne peut pas lui enlever son mute donc on le préviens
        newVoiceState.member.user.send(new Discord.MessageEmbed().setTitle("Vous vous êtes déconnecté alors que vous étiez mute").setDescription("N'étant plus dans un channel vocal, je ne peut pas vous retirer le mute serveur que vous aviez.\n Pas de panique, plusieurs solutions :\n\n**Retourner en partie**\nRetournez dans le channel vocal de la partie pour la finir avec les autres joueurs.\n**La commande =unmute**\nConnectez-vous dans un channel vocal du server et éxecutez-la (seulement si je suis toujours en ligne d'ici là).\n**Contacter une personne ayant les droits**\nMoi je ne connais personne, désolé :)").setColor(couleurEmbedError));
      }
    }
  }
}

/*
UTILITÉ
Ce listeer activité uniquement pendant les périodes de jour du jeu permet de mute les joueurs rejoignant la partie pendant cette période s'ils ne sont pas des joueurs ou s'ils sont mirts et de les unmutes s'ils quittent le vocal alors qu'ils sont mute
En réalité, il est activé lors de jin/leave et mute/unmute sur channel vocal -> c'est la raison de la condition avec mute
PARAMÈTRES
oldVoiceState -> (Voice state) L'état vocal du guildMemeber qui change/se déconnecte de channel vocal ou se mute/unmute avant le changement en question
newVoiceState -> (Voice state) L'état vocal du guildMemeber qui change/se déconnecte de channel vocal ou se mute/unmute après le changement en question
*/
function listenerVoiceUpdateJour(oldVoiceState, newVoiceState){
  if(partieEnCours){
    const newVoiceStateChannel = newVoiceState.channel;
    const oldVoiceStateChannel = oldVoiceState.channel;

    if((newVoiceStateChannel != oldVoiceStateChannel) && !newVoiceState.member.user.bot){ // L'event n'est pas lancé pour une raison de mute/unmute mais bel et bien pour un changement de channel et ce n'est pas du bot que l'on parle
      if(newVoiceStateChannel == channelVocalVillage){ // Il a rejoint un channel vocal et c'est le channel vocal du village
        if(lesJoueurs.get(newVoiceState.member.user) != undefined){ // C'est un joueur de la partie
          if(lesMorts.get(newVoiceState.member.user) != undefined){ // Il est mort
            newVoiceState.member.voice.setMute(true);
          }else{ // Il n'est pas mort
            newVoiceState.member.voice.setMute(false);
          }
        }else{ // C'est pas un joueur de la partie
          newVoiceState.member.voice.setMute(true);
        }
      }else if(oldVoiceStateChannel == channelVocalVillage && newVoiceStateChannel != undefined){ // Il change de channel vocal à partir du voc village
        newVoiceState.member.voice.setMute(false);
      }else if(oldVoiceStateChannel == channelVocalVillage && newVoiceStateChannel == undefined){ // Il s'est déconnecté, on ne peut pas lui enlever son mute donc on le préviens
        newVoiceState.member.user.send(new Discord.MessageEmbed().setTitle("Vous vous êtes déconnecté alors que vous étiez mute").setDescription("N'étant plus dans un channel vocal, je ne peut pas vous retirer le mute serveur que vous aviez.\n Pas de panique, plusieurs solutions :\n\n**Retourner en partie**\nRetournez dans le channel vocal de la partie pour la finir avec les autres joueurs.\n**La commande =unmute**\nConnectez-vous dans un channel vocal du server et éxecutez-la (seulement si je suis toujours en ligne d'ici là).\n**Contacter une personne ayant les droits**\nMoi je ne connais personne, désolé :)").setColor(couleurEmbedError));
      }
    }
  }
}

/*
UTILITÉ
La partie communne entre une nuit lambda et la première
*/
async function debutNuit(){
  nbNuits ++; // Pour les logs
  evenement("lune.png", "Annonce de la nuit n°"+nbNuits, "nuit"); // Pour les logs

  // On met le listener du vocal de la nuit et on enlève celui du jour
  bot.removeListener('voiceStateUpdate', listenerVoiceUpdateJour);
  bot.on('voiceStateUpdate', listenerVoiceUpdateNuit);

  channelTextuelVillage.send(new Discord.MessageEmbed().setTitle(":first_quarter_moon: Le serveur s'endort, les joueurs se mutent :last_quarter_moon: "));

  // On mute les joueurs avec certaines conditions
  for(let pers of channelVocalVillage.members){
    if( !pers[1].user.bot){ // Il y a juste le bot qui n'est pas mute la nuit
      pers[1].voice.setMute(true);
    }
  }

  // Le bot joue la nuit et se connecte s'il ne l'est pas déjà
  if(! memberBot.voice.channel || memberBot.voice.channel != channelVocalVillage){
    await channelVocalVillage.join().then(function(connection){
      connectionVocaleBot = connection;
    });
  }
  connectionVocaleBot.play(bot.ytdl('https://www.youtube.com/watch?v=GpfY2P1mXr8', { quality: 'highestaudio'})); // Il ne faut pas que la durée de la video soit trop importante

  // On enlève le droit d'écire au village
  await changerPermissionEcritureVillage(false);
}

/*
UTILITÉ
La partie communne entre un jour lambda et le premier
*/
async function debutJour(){
  nbJours ++; // Pour les logs
  evenement("soleil.svg", "Annonce du jour n°"+nbJours, "jour"); // Pour les logs

  // On met le listener du vocal du jour et on enlève celui de la nuit
  bot.removeListener('voiceStateUpdate', listenerVoiceUpdateNuit);
  bot.on('voiceStateUpdate', listenerVoiceUpdateJour);

  channelTextuelVillage.send(new Discord.MessageEmbed().setTitle(":sunny: Le serveur se démute... euh se réveille... :sunny:"));

  // On démute les joueurs avec certaines conditions
  for(let pers of channelVocalVillage.members){
    if( !pers[1].user.bot && lesJoueurs.get(pers[1].user) != undefined && lesMorts.get(pers[1].user) == undefined){ // Pour être unmute, il ne faut pas être un bot, il faut être un joueur non mort
      pers[1].voice.setMute(false);
    }
  }

  // Le bot joue le réveil et se connecte s'il ne l'est pas déjà
  if(! memberBot.voice.channel || memberBot.voice.channel != channelVocalVillage){
    await channelVocalVillage.join().then(function(connection){
      connectionVocaleBot = connection;
    });
  }
  connectionVocaleBot.play(bot.ytdl('https://www.youtube.com/watch?v=dbVfi3nLA20', {quality: 'highestaudio'}));

  // On met le droit d'écrire au village
  await changerPermissionEcritureVillage(true);
}

/*
UTILITÉ
Retourne true si une équipe à remplie les conditions de vitcoire sinon retourne false
*/
function partieFinie(){
  if(lesJoueurs.size <= 0){ // Victoire personne
    return "vp";
  }else{
    if((lesJoueurs.size == 2) && (lesAmoureux != []) && (lesJoueurs.get(lesAmoureux[0]) != undefined) && (lesJoueurs.get(lesAmoureux[1]) != undefined)) { // Victoire de des amoureux
      return "va";
    }else{
      if(listeLoups.length == 0){// Victoire du village
        return "vv";
      }else if(listeLoups.length == lesJoueurs.size){ // Victoire des loups
        return "vl";
      }
    }
    return "continuer"; // Il n'y a pas de vitoire
  }
}

/*
UTILITÉ
En cas de morts durant la nuit, cette fonction va afficher, la matin venu, leurs rôles et en fonction de celui-ci, effectuer différentes actions (passation de pouvoir etc ...)
*/
function verifierMortsDeLaNuit(){
  return new Promise(async function(resolve) {
    for(let user of mortsDeLaNuit){
      await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle(":skull: "+user.username+" était... :skull:")).then(async function(message){
         await supprJoueurMort(message, user);
      });
    }
    mortsDeLaNuit = [];
    resolve();
  });
}

/*
UTILITÉ
Appel à la supression du joueurs lors d'un mort durant la nuit -> Pour l'affichage de son role
Le seul intérêt de séparer cette fonction de la fonction verifierMortsDeLaNuit() est la promise que l'on rajoute ici
PARAMÈTRES
message -> le message annoncant la mort à supprimer
user -> Le joueur à supprimer
*/
function supprJoueurMort(message, user){
  return new Promise(function(resolve) {
    lesJoueurs.get(user)[2].voice.setMute(true); // On le mute (avant l'annonce de son rôle pour éviter la diffusion d'informations) pour le restant de la partie
    setTimeout(async function(){
      message.delete();
      await retirerJoueur(user);
      resolve();
    }, tempsAvantAnnonceRolePersonneElimine*1000);
  });
}

/*
UTILITÉ
Est utilisée plusieurs fois dans le code à lidentique -> sert à retourner la liste des joueurs avec les lettre bleues à coté (pour les votes)
PARAMÈTRE
loup -> un boolean indiquant si il faut retirer de la liste les loups ou non
capi -> un boolean indiquant si il faut retirer de la liste le capitaine ou non
chas -> un boolean indiquant si il faut retirer de la liste le chasseur ou non
*/
function getListeVillageAvecLettres(loups=false, capi=false, chas=false){
  let textEmbed = '';
  let i = 0;
  let lesMembres = [];
  for(let [user, liste] of lesJoueurs){ // !loups = on les gardes
    if((user != capitaine || !capi) && (liste[0] != "Loup-garou" || !loups) && (liste[0] != "Chasseur" || !chas)){ // Si loups est à true, on les retire de la liste finale des joueurs. Pareil pour capi et chas
      textEmbed += lesLettres.get(i) + ' ' + user.username + '\n';
      lesMembres.push(user); // User
      i++;
    }
  }
  return [textEmbed, lesMembres];
}

/*
PARAMÈTRE
joueur: un User contenant le joueur qui est mort
UTILITÉ
Retire le joueur du jeu
*/
function retirerJoueur(joueur){
  return new Promise(async function(resolve){
    // Le bot joue le son de mort et se connecte s'il ne l'est pas déjà
    if(! memberBot.voice.channel || memberBot.voice.channel != channelVocalVillage){
      await channelVocalVillage.join().then(function(connection){
        connectionVocaleBot = connection;
      });
    }
    connectionVocaleBot.play(bot.ytdl('https://www.youtube.com/watch?v=d7VYVax53PI', {quality: 'highestaudio'}));

    let j = lesJoueurs.get(joueur);

    // Le joueur est mute pour le reste de la partie avant l'exécution de cette fonction pour que dès l'annonce de sa mort au village, il ne puisse pas divulguer d'informations.

    await expliquerRole(j[0], channelTextuelVillage);
    channelTextuelVillage.updateOverwrite(j[1], {SEND_MESSAGES : false});
    channelMort.updateOverwrite(j[1], { VIEW_CHANNEL: true });

    separateur(); // Pour les logs
    switch (j[0]){
      case "Petite-fille":
        evenementBulle("mort.svg", "<b>La Petite-Fille | "+joueur.username+"</b> décède"); // Pour les logs
        infoPartie.Role.petiteFille.nb = 0; // Le nombre est mis à 0 car le rôle n'est sensé apparaitre qu'une fois
        petiteFille = undefined;
        channelTextuelPetiteFille.delete("Mort de la petite fille");
        channelTextuelPetiteFille = undefined;
        break;
      case "Chasseur":
        await tirChasseur();
        infoPartie.Role.chasseur.nb = 0; // Le nombre est mis à 0 car le rôle n'est sensé apparaitre qu'une fois
        break;
      case "Cupidon":
        evenementBulle("mort.svg", "<b>Cupidon | "+joueur.username+"</b> décède"); // Pour les logs
        infoPartie.Role.cupidon.nb = 0; // Le nombre est mis à 0 car le rôle n'est sensé apparaitre qu'une fois
        cupidon = undefined;
        channelTextuelCupidon.delete("Mort de cupidon");
        channelTextuelCupidon = undefined;
        break;
      case "Sorciere":
        evenementBulle("mort.svg", "<b>La Sorcière | "+joueur.username+"</b> décède"); // Pour les logs
        infoPartie.Role.sorciere.nb = 0; // Le nombre est mis à 0 car le rôle n'est sensé apparaitre qu'une fois
        sorciere = undefined;
        channelTextuelSorciere.delete("Mort de la sorcière");
        channelTextuelSorciere = undefined;
        break;
      case "Voyante":
        evenementBulle("mort.svg", "<b>La Voyante | "+joueur.username+"</b> décède"); // Pour les logs
        infoPartie.Role.voyante.nb = 0; // Le nombre est mis à 0 car le rôle n'est sensé apparaitre qu'une fois
        voyante = undefined;
        channelTextuelVoyante.delete("Mort de la voyante");
        channelTextuelVoyante = undefined;
        break;
      case "Medium":
        evenementBulle("mort.svg", "<b>Le Medium | "+joueur.username+"</b> décède"); // Pour les logs
        infoPartie.Role.medium.nb = 0; // Le nombre est mis à 0 car le rôle n'est sensé apparaitre qu'une fois
        medium = undefined;
        channelTextuelMedium.delete('Mort du Medium');
        channelTextuelMedium = undefined;
        break;
      case "Loup-garou":
        listeLoups.splice(listeLoups.indexOf(joueur), 1); // Retirer le loup de la liste
        if(listeLoups.length == 0){
          evenementBulle("mort.svg", "Le dernier <b>Loup-Garou | "+joueur.username+"</b> décède"); // Pour les logs
          infoPartie.Role.loupGarou.nb --;
          channelTextuelLoupsGarous.delete("Morts de tous les Loups-Garous");
          channelTextuelLoupsGarous = undefined;
        }else{
          evenementBulle("mort.svg", "<b>Un Loup-Garou | "+joueur.username+"</b> décède (Ils ne sont plus que "+listeLoups.length+")"); // Pour les logs
          channelTextuelLoupsGarous.updateOverwrite(j[1], { VIEW_CHANNEL: false });
        }
        break;
      case "Villageois":
        evenementBulle("mort.svg", "<b>Un Villageois | "+joueur.username+"</b> décède"); // Pour les logs
        infoPartie.Role.villageois.nb --;
        break;
      default:
        console.log("fonction retirer joueur -> role non reconnu : "+j[0]);
        infoPartie.Role.autre.nb --;
    }

    bot.fs.writeFile("./jeux/loup_garou/tableau/infoPartie.json", JSON.stringify(infoPartie, null, 4),err => { //on écrit dans le fichier 'infoPartie.json' les informations précédentes
      if(err) throw err;
    });

    if(capitaine && capitaine == joueur){
      separateur(); // Pour les logs
      await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle(":military_medal: Et ce n'est pas tout :military_medal:").setDescription("N'oublions pas que **"+joueur.username+"** est **Capitaine** : respectant son devoir jusqu'au bout, il nomme donc un successeur parmis les survivants").setColor(couleurEmbedInfos)).then(async function(m){
        await new Promise(function(reso){
          setTimeout(async function(){
            await definirSuccesseurCapitaine();
            reso();
          }, tempsAvantAnnonceRolePersonneElimine*1000);
        });
      });
    }

    if(lesAmoureux && lesAmoureux.includes(joueur)){
      separateur(); // Pour les logs
      let autre;
      if(lesAmoureux[0] == joueur){
        autre = lesAmoureux[1];
      }else{
        autre = lesAmoureux[0];
      }

      evenementBulle("mort.svg", "<b>"+joueur.username+"</b> est amoureux de "+autre.username+" ("+lesJoueurs.get(autre)[0]+") : ce dernier décède de chagrin"); // Pour les logs
      lesAmoureux = [];

      await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle(":heartbeat: Et ce n'est pas tout :heartbeat:").setDescription(joueur.username+" est tombé fou amoureux de quelqu'un durant la partie : **"+autre.username+"**").footer("Étant un gentlemen, "+joueur.username+" prend au pied de la lettre 'À la vie, à la mort' et emporte dans la tombe "+autre.username+" qui était ...").setColor(couleurEmbedInfos)).then(async function(m){
        m.react("💘");
        await new Promise(function(reso){
          setTimeout(async function(){
            await retirerJoueur(autre);
            reso();
          }, tempsAvantAnnonceRolePersonneElimine*1000);
        });
      });
    }

    if(medium){
      channelTextuelMediumMort.updateOverwrite(j[1], { VIEW_CHANNEL: true });
    }

    lesMorts.set(joueur, [ "", lesJoueurs.get(joueur)[1], lesJoueurs.get(joueur)[2], valeurTimerGlobal.valueOf()]);
    lesJoueurs.delete(joueur);
    resolve();
  });
}

//==============================================================================
// PERSMISSIONS
//==============================================================================

/*
PARAMÈTRE
channel: le channel concerné
role: String du role
stringRole: Dictionnaire avec (Role)role -> (String)role
UTILITÉ
Creation d'un channel textuel pour un role du loup-garou partagé à tous les joueurs ayant ce rôle
*/
function changerPermission(channel, role, stringRole){
  for(let servRole of guildeCourante.roles.cache.values()){
    if(stringRole.get(servRole) == role && (role == "Loup-garou" || role == "Petite-fille" || role == "Cupidon" || role == "Voyante" || role == "Sorciere" || role == "Medium")) { // Pour interdire les roles cités d'écrire dans leur channel
      channel.updateOverwrite(servRole, { VIEW_CHANNEL: true, SEND_MESSAGES: false });
    }else if (stringRole.get(servRole) == role) {
      channel.updateOverwrite(servRole, { VIEW_CHANNEL: true });
    }else{
      channel.updateOverwrite(servRole, { VIEW_CHANNEL: false });
    }
  }
}

/*
channel: soit le channel textuel soit le channel vocal du village
stringRole: Dictionnaire avec (Role)role -> (String)role
UTILITÉ
limite l'accès au channel du village aux joueurs (et aux admins)
*/
async function changerPermissionVillage(channel, stringRole){
  for(let servRole of guildeCourante.roles.cache.values()){
    if(stringRole.has(servRole)){
      await channel.updateOverwrite(servRole, { VIEW_CHANNEL: true });
    }else{
      await channel.updateOverwrite(servRole, { VIEW_CHANNEL: false });
    }
  }
}

/*
bool: le boolean correspondant à l'autorisation ou non d'écrire
UTILITÉ
Permettre au loups-garous de discuter entre eux via leur channel textuel
*/
async function changerPermissionEcritureLoups(bool){
  for(let loup of listeLoups){
    await channelTextuelLoupsGarous.updateOverwrite(lesJoueurs.get(loup)[1], {'SEND_MESSAGES' : bool});
  }
}

/*
bool: le boolean correspondant à l'autorisation ou non d'écrire pour le village
UTILITÉ
Permettre au village de discuter entre eux via leur channel textuel
*/
function changerPermissionEcritureVillage(bool){
  return new Promise(async function(resolve) {
    for(let [user, liste] of lesJoueurs){
      await channelTextuelVillage.updateOverwrite(liste[1], {'SEND_MESSAGES' : bool});
    }
    resolve();
  });
}

/*
bool : le boolean a vrai si les fonctionnalités du médium doivent être activé, à false si elle doivent être désactivée
UTILITÉ
Permettre aux morts d'envoyer des messages au médium et permettre au médium de recevoir les messages
*/
async function changerPermissionMediumMort(bool){
  for (let [user, liste] of lesMorts) {
    await channelTextuelMediumMort.updateOverwrite(liste[1], {'SEND_MESSAGES' : bool});
    await channelTextuelMediumMort.setRateLimitPerUser(4); // temps en seconde de slowmode sur le channel (ne s'applique pas aux administrateur du serveur)
  }
}

/*
UTILITÉ
Regarde que la position du role soit bien plus haute que celle de plus haut rôle des joueurs
*/
function verifierPositionnerRoleBot(listePersonnes){
  let posOriginale = rolePrincipalBot.position;
  let max = posOriginale;
  let nomRole;
  for (var user of listePersonnes) { // parcours des joueurs
    let membreGuilde = guildeCourante.member(user);
    let tmpMax = max;
    let tmpNom = nomRole;
    let aAdmin = false;
    for (var [id, data] of membreGuilde.roles.cache) { // parcours des roles
      if (data.permissions.has("ADMINISTRATOR")){
        aAdmin = true;
        break
      }
      else if(data.position > tmpMax){
        tmpMax = data.position;
        tmpNom = data.name;
      }
    }
    if (!aAdmin) {
      max = tmpMax;
      nomRole = tmpNom;
    }
  }
  console.log("bot : " + posOriginale + ", max : " + max + " aka " + nomRole);
  if(posOriginale < max){
    channelPrincipal.send("Je viens de vérifier et il se trouve que la position de mon rôle est inférieur à la position du rôle le plus haut des joueurs. Le rôle dont je parle est le suivant : **" + nomRole + "**.\n Veuillez contacter un administrateur pour qu'il réhausse mon rôle dans la hierarchie sans quoi certaines fonctionnalités risque de ne fonctionneront pas totalement !");
  }
}
