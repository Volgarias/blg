//==============================================================================
// Tableau des roles présents dans une partie
//==============================================================================


/*
UTILITÉ
Initialiser le dictionnaire qui sera inscrit dans le fichier infoPartie.json servant à l'affichage du tableau sur la page web
*/
function RAZJson(){
  infoPartie.nomServeur = guildeCourante.name;
  infoPartie.Role = { villageois :  {nb: 0, present: 0, nom: "Villageoi(s)",  image: "Villageois.png"},
                      loupGarou :   {nb: 0, present: 0, nom: "Loup-Garou(s)", image: "Loup-garou.png"},
                      sorciere :    {nb: 0, present: 0, nom: "Sorcière",      image: "Sorciere.png"},
                      petiteFille : {nb: 0, present: 0, nom: "Petite-fille",  image: "Petite-fille.png"},
                      voyante :     {nb: 0, present: 0, nom: "Voyante",       image: "Voyante.png"},
                      chasseur :    {nb: 0, present: 0, nom: "Chasseur",      image: "Chasseur.png"},
                      cupidon :     {nb: 0, present: 0, nom: "Cupidon",       image: "Cupidon.png"},
                      medium :      {nb: 0, present: 0, nom: "Médium",        image: "Medium.png"},
                      autre :       {nb: 0, present: 0, nom: "Autre",         image: "../Verso.png"}};
  bot.fs.writeFile("./jeux/loup_garou/tableau/infoPartie.json", JSON.stringify(infoPartie, null, 4), err => { //on écrit dans le fichier 'infoPartie.json' les informations précédentes
    if(err) throw err;
  });
}

/*
UTILITÉ
Créé le tableau des rôles présents dans la partie qui est publié en ligne pour permettre l'accès publique
*/
function genererTableauRoles(){
  return new Promise(async function(resolve) {
    await serveurLocal();
    const url = await bot.ngrok.connect(portServeur);
    lienSite = url;
    console.log(url+"/joueurs.html");
    resolve();
  });
}

/*
UTILITÉ
Créé le serveur local avec le tableau des participants qui sera ensuite affiché en ligne
*/
function serveurLocal(){
  return new Promise(function(resolve){
    const app = bot.express();

    app.use(bot.express.static('./jeux/loup_garou/tableau/')); // Pouvoir utiliser les documents du dossier tableau
    app.use(bot.express.static('./jeux/loup_garou/images_roles/')); // Pouvoir utiliser les images des rôles

    serveur = app.listen(portServeur, function(){
      resolve();
    });
  });
}
