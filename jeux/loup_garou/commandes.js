//==============================================================================
// Les commandes du bot pour lancer le jeu
//==============================================================================


/*
UTILITÉ
Traite une commande pour le jeu loup-garou
PARAMÈTRE
message -> le message d'un utilisateur
*/
async function commandeLoupGarou(message){
  message.delete();

  if(BETA && (message.channel.type == "dm" || message.guild.members.cache.find(guilMember => guilMember.user.id == message.author.id).roles.cache.find(role => role.name == "ManageBLG") == undefined)){
    message.author.send(new Discord.MessageEmbed().setTitle('Désolé '+message.author.username).setDescription('Je ne serait pas utilisable avant *Lundi soir* pour les personnes sans le rôle *ManageBLG*').setFooter('Ne soit pas trop pressé(e) voyons :D'));
  }else{
    switch(message.content){
      case "=start":
        if(! partieEnCours){
          channelPrincipal = message.channel;
          guildeCourante = message.guild;
          memberBot = guildeCourante.members.cache.get(bot.user.id);
          rolePrincipalBot = guildeCourante.members.cache.find(guilMember => guilMember.user.id == bot.user.id).roles.cache.find(role => role.managed == true);
          lancerPartie();
        }else{
          message.channel.send("Une partie est déjà en cours !").then(function(newMessage){setTimeout(function(){newMessage.delete();}, tempsAvantDeleteMessageInformations*1000)}); // Supression du message
        }
        break;
      case "=stop":
        if(partieEnCours){
          if(message.channel.guild == guildeCourante){
            if(guildeCourante.members.cache.find(guilMember => guilMember.user.id == message.author.id).roles.cache.find(role => role.name == "ManageBLG") != undefined){ // Si la commande est bien envoyé depuis le serveur ayant lancé la partie et que la personne l'ayant lancé à bien le role requis pour manage le bot
              arreterPartieEncours();
            }else{
              message.channel.send("Il te faut un role nommé 'ManageBLG' pour que tu ai le droit d'arrêter une partie avant la fin normale de celle-ci").then(function(newMessage){setTimeout(function(){newMessage.delete();}, tempsAvantDeleteMessageInformations*1000)}); // Supression du message
            }
          }else{
            message.channel.send("Désolé mais la partie que vous tentez d'arrêter a été commencé sur un autre serveur : "+guildeCourante.name+".\nLancer cette commande depuis ce serveur résolvera le problème.").then(function(newMessage){setTimeout(function(){newMessage.delete();}, tempsAvantDeleteMessageInformations*1000)}); // Supression du message
          }
        }else{
          message.channel.send("Aucune partie n'est encore lancée").then(function(newMessage){setTimeout(function(){newMessage.delete();}, tempsAvantDeleteMessageInformations*1000)}); // Supression du message
        }
        break;
      case "=regles":
        await expliquerRegles(message.author);
        break;
      case "=role":
        if(partieEnCours && lesJoueurs.get(message.author) != null){
          await expliquerRole(lesJoueurs.get(message.author)[0], message.author);
        }else{
          message.channel.send("Tu n'as pas de rôle !").then(function(newMessage){setTimeout(function(){newMessage.delete()}, tempsAvantDeleteMessageInformations*1000)});
        }
        break;
      case "=help":
        help(message.channel);
        break;
      case "=perms":
        if(! partieEnCours){ // Pour éviter de pouvoir arrêter la partie depuis un autre serveur car la variables guildeCourante est changée ici comme dans le start
          channelPrincipal = message.channel;
          guildeCourante = message.guild;
          rolePrincipalBot = guildeCourante.members.cache.find(guilMember => guilMember.user.id == bot.user.id).roles.cache.find(role => role.managed == true);
          verifierPermissions(true);
        }else{
          message.channel.send("Une partie est en cours !").then(function(newMessage){setTimeout(function(){newMessage.delete();}, tempsAvantDeleteMessageInformations*1000)}); // Supression du message
        }
        break;
      case "=unmute":
        let member = message.guild.members.cache.get(message.author.id);
        if(member.voice.channel){
          if(partieEnCours && lesJoueurs.get(member.user) != undefined && member.voice.channel == channelVocalVillage){
            message.channel.send("Vous êtes avec les autres joueurs d'une partie en cours, je ne peut pas vous unmute").then(function(newMessage){setTimeout(function(){newMessage.delete()}, tempsAvantDeleteMessageInformations*1000)});
          }else{
            member.voice.setMute(false);
            message.channel.send("Vous avez bien été unmute").then(function(newMessage){setTimeout(function(){newMessage.delete()}, tempsAvantDeleteMessageInformations*1000)});
          }
        }else{
          message.channel.send("Vous devez être connecté à un channel vocal").then(function(newMessage){setTimeout(function(){newMessage.delete()}, tempsAvantDeleteMessageInformations*1000)});
        }
        break;
      case "=test":
        console.log("rien");
        break;
      default:
        message.channel.send("La commande n'a pas été comprise, tape '=help' pour avoir un apperçu de celles-ci").then(function(newMessage){setTimeout(function(){newMessage.delete()}, tempsAvantDeleteMessageInformations*1000)});
    }
  }
}


//==============================================================================
// FONCTIONS APPELÉES
//==============================================================================

/*
UTILITÉ
Lancer un sondage permettant de remplir la liste des participants au prochain loup garou
*/
function lancerPartie(){
  partieEnCours = true;

  channelPrincipal.send(new Discord.MessageEmbed().setTitle("Nouvelle partie").setDescription("Qui veut participer à la prochaine partie de Loup-Garou ?").addField("Temps restant", tempsAttenteAvantFinSondage+"s").setColor(couleurEmbedSondage)).then(function(sondage){
    sondage.react("✋");// On met une réaction pour faire voter les gens

    let cpt = 0;
    let timer = setInterval(function(){
      if(cpt == tempsAttenteAvantFinSondage){

        clearInterval(timer); // On arrete le décompte
        let reaction = sondage.reactions.cache.get("✋");

        // if(reaction.count>1 && reaction.count<4){
        //   sondage.edit(new Discord.MessageEmbed().setTitle("Pas assez de monde").setDescription("Apparemment il n'y a pas assez de personne pour lancer la partie").setFooter("La partie est annulée").setColor(couleurEmbedError));
        //   partieEnCours = false;
        if(reaction.count>1){ // Si il y a plus d'une personne qui vote (plus que juste le bot)
          let listePersonnes = [];
          for(let elem of reaction.users.cache){
            if(!elem[1].bot){
              listePersonnes.push(elem[1]); // On récupère les personnes qui participent
            }
          }
          verifierPositionnerRoleBot(listePersonnes);
          if(verifierPermissions()){ // Si le bot à les droits nécéssaires à la partie
            timerGlobal = setInterval(function(){valeurTimerGlobal ++;}, 1000);
            RAZJson();
            choixRoleRandom(listePersonnes); // On assigne les roles
          }else{
            channelPrincipal.send("Je n'ai pas assez de droits pour lancer une partie, tapez =perms pour tenter de résoudre le problème");
          }
        }else{
          sondage.edit(new Discord.MessageEmbed().setTitle("😭 Y'a personne 😭").setDescription("Apparemment personne ne rejoint").setFooter("La partie est annulée").setColor(couleurEmbedError));
          partieEnCours = false;
        }
        setTimeout(function(){sondage.delete();}, tempsAvantDeleteMessageInformations*1000); // Effacer le message de sondage au bout de 3 secondes
      }else{
        sondage.edit(new Discord.MessageEmbed().setTitle("Nouvelle partie").setDescription("Qui veut participer à la prochaine partie de Loup-Garou ?").addField("Temps restant", (tempsAttenteAvantFinSondage-cpt)+"s").setColor(couleurEmbedSondage));
      }
      cpt ++;
    }, 1000); // Éxécution toutes les secondes
  });
}

/*
UTILITÉ
Arrêter la partie en cours
*/
async function arreterPartieEncours(){
  partieEnCours = false;
  // Roles
  if(fonctionStop.get("roles") != []){
    for(let role of fonctionStop.get("roles")){
      if(! role.deleted){
        await role.delete("Fin de la partie");
      }
    }
  }

  // Messages
  if(fonctionStop.get("messages") != []){
    for(let message of fonctionStop.get("messages")){
      if(! message.deleted){
        await message.delete();
      }
    }
  }

  await bot.ngrok.disconnect();
  serveur.close();

  if(capitaine){
    if(! lesJoueurs.get(capitaine)[2].hasPermission("ADMINISTRATOR") && lesJoueurs.get(capitaine)[2].nickname && lesJoueurs.get(capitaine)[2].nickname[0] == "🎖" && lesJoueurs.get(capitaine)[2].nickname.length>=2){
      lesJoueurs.get(capitaine)[2].setNickname(lesJoueurs.get(capitaine)[2].nickname.substr(1));
    }
    capitaine = undefined;
  }

  lesJoueurs           = new Map();
  lesAmoureux          = undefined;
  petiteFille          = undefined;
  chasseur             = undefined;
  cupidon              = undefined;
  sorciere             = undefined;
  voyante              = undefined;
  medium               = undefined
  listeLoups           = [];
  mortsDeLaNuit        = [];
  listePotionsSorciere = [true, true];

  // Salons
  if(fonctionStop.get("salons") != []){
    for(let salon of fonctionStop.get("salons")){
      if(!salon.deleted){ // Le salon a pu être supprimé auparavant comme celui de Cupidon
        await salon.delete("Fin de la partie");
      }
    }
  }
  channelTextuelPetiteFille = undefined;
  channelTextuelSorciere    = undefined;
  channelTextuelCupidon     = undefined;
  channelTextuelVoyante     = undefined;
  channelTextuelVillage     = undefined;
  channelVocalVillage       = undefined;
  channelTextuelMedium      = undefined;
  channelTextuelMediumMort  = undefined;

  // Catégorie
  await categoryChannelLG.delete("Fin de la partie");
  categoryChannelLG = undefined;
  fonctionStop = new Map([["roles", []], ["salons", []], ["messages", []]]); // Dictionnaire pour la supression des channels, des rôles etc...

  RAZJson();
}

/*
UTILITÉ
Détermine si oui ou non le bot a assez de roles pour lancer la partie
PARAMÈTRE
envoyerMessage -> boolean représentant la nécéssité d'envoyer le résultat de la vérification dans le channel de départ du jeu ou non
*/
function verifierPermissions(envoyerMessage=false){
  let res = true;
  let message = "**Etat possible des permissions**\n:green_square: **Requise et mise**    | :red_square: **Requise et manquante** \n:orange_square: **Non requise et mise** | :blue_square: **Non requise et non mise**\n\n*Le droit administrator est requis le temps qu'une optimisation soit effectuée*\n";
  if(rolePrincipalBot.permissions.has("ADMINISTRATOR"))      { message += ":green_square: ADMINISTRATOR \n";}        else {res = false; message += ":red_square: ADMINISTRATOR \n";}
     rolePrincipalBot.permissions.has("CREATE_INSTANT_INVITE") ? message += ":orange_square: CREATE_INSTANT_INVITE \n" :     message += ":blue_square: CREATE_INSTANT_INVITE \n"
     rolePrincipalBot.permissions.has("KICK_MEMBERS")          ? message += ":orange_square: KICK_MEMBERS \n"          :     message += ":blue_square: KICK_MEMBERS \n"
     rolePrincipalBot.permissions.has("BAN_MEMBERS")           ? message += ":orange_square: BAN_MEMBERS \n"           :     message += ":blue_square: BAN_MEMBERS \n"
  if(rolePrincipalBot.permissions.has("MANAGE_CHANNELS"))      { message += ":green_square: MANAGE_CHANNELS \n";}      else {res = false; message += ":red_square: MANAGE_CHANNELS \n";}
  if(rolePrincipalBot.permissions.has("MANAGE_GUILD"))         { message += ":green_square: MANAGE_GUILD \n";}         else {res = false; message += ":red_square: MANAGE_GUILD \n";}
  if(rolePrincipalBot.permissions.has("ADD_REACTIONS"))        { message += ":green_square: ADD_REACTIONS \n";}        else {res = false; message += ":red_square: ADD_REACTIONS \n";}
     rolePrincipalBot.permissions.has("VIEW_AUDIT_LOG")        ? message += ":orange_square: VIEW_AUDIT_LOG \n"        :     message += ":blue_square: VIEW_AUDIT_LOG \n"
     rolePrincipalBot.permissions.has("PRIORITY_SPEAKER")      ? message += ":orange_square: PRIORITY_SPEAKER \n"      :     message += ":blue_square: PRIORITY_SPEAKER \n"
     rolePrincipalBot.permissions.has("STREAM")                ? message += ":orange_square: STREAM \n"                :     message += ":blue_square: STREAM \n"
  if(rolePrincipalBot.permissions.has("VIEW_CHANNEL"))         { message += ":green_square: VIEW_CHANNEL \n";}         else {res = false; message += ":red_square: VIEW_CHANNEL \n";}
  if(rolePrincipalBot.permissions.has("SEND_MESSAGES"))        { message += ":green_square: SEND_MESSAGES \n";}        else {res = false; message += ":red_square: SEND_MESSAGES \n";}
     rolePrincipalBot.permissions.has("SEND_TTS_MESSAGES")     ? message += ":orange_square: SEND_TTS_MESSAGES \n"     :     message += ":blue_square: SEND_TTS_MESSAGES \n"
  if(rolePrincipalBot.permissions.has("MANAGE_MESSAGES"))      { message += ":green_square: MANAGE_MESSAGES \n";}      else {res = false; message += ":red_square: MANAGE_MESSAGES \n";}
  if(rolePrincipalBot.permissions.has("EMBED_LINKS"))          { message += ":green_square: EMBED_LINKS \n";}          else {res = false; message += ":red_square: EMBED_LINKS \n";}
  if(rolePrincipalBot.permissions.has("ATTACH_FILES"))         { message += ":green_square: ATTACH_FILES \n";}         else {res = false; message += ":red_square: ATTACH_FILES \n";}
  if(rolePrincipalBot.permissions.has("READ_MESSAGE_HISTORY")) { message += ":green_square: READ_MESSAGE_HISTORY \n";} else {res = false; message += ":red_square: READ_MESSAGE_HISTORY \n";}
     rolePrincipalBot.permissions.has("MENTION_EVERYONE")      ? message += ":orange_square: MENTION_EVERYONE \n"      :     message += ":blue_square: MENTION_EVERYONE \n"
     rolePrincipalBot.permissions.has("USE_EXTERNAL_EMOJIS")   ? message += ":orange_square: USE_EXTERNAL_EMOJIS \n"   :     message += ":blue_square: USE_EXTERNAL_EMOJIS \n"
  // if(rolePrincipalBot.permissions.has("VIEW_GUILD_INSIGHTS"))  { message += ":green_square: VIEW_GUILD_INSIGHTS \n";}  else {res = false; message += ":red_square: VIEW_GUILD_INSIGHTS \n";}
  if(rolePrincipalBot.permissions.has("CONNECT"))              { message += ":green_square: CONNECT \n";}              else {res = false; message += ":red_square: CONNECT \n";}
  if(rolePrincipalBot.permissions.has("SPEAK"))                { message += ":green_square: SPEAK \n";}                else {res = false; message += ":red_square: SPEAK \n";}
  if(rolePrincipalBot.permissions.has("MUTE_MEMBERS"))         { message += ":green_square: MUTE_MEMBERS \n";}         else {res = false; message += ":red_square: MUTE_MEMBERS \n";}
     rolePrincipalBot.permissions.has("DEAFEN_MEMBERS")        ? message += ":orange_square: DEAFEN_MEMBERS \n"        :     message += ":blue_square: DEAFEN_MEMBERS \n"
  if(rolePrincipalBot.permissions.has("MOVE_MEMBERS"))         { message += ":green_square: MOVE_MEMBERS \n";}         else {res = false; message += ":red_square: MOVE_MEMBERS \n";}
     rolePrincipalBot.permissions.has("USE_VAD")               ? message += ":orange_square: USE_VAD \n"               :     message += ":blue_square: USE_VAD \n"
     rolePrincipalBot.permissions.has("CHANGE_NICKNAME")       ? message += ":orange_square: CHANGE_NICKNAME \n"       :     message += ":blue_square: CHANGE_NICKNAME \n"
  if(rolePrincipalBot.permissions.has("MANAGE_NICKNAMES"))     { message += ":green_square: MANAGE_NICKNAMES \n";}     else {res = false; message += ":red_square: MANAGE_NICKNAMES \n";}
  if(rolePrincipalBot.permissions.has("MANAGE_ROLES"))         { message += ":green_square: MANAGE_ROLES \n";}         else {res = false; message += ":red_square: MANAGE_ROLES \n";}
     rolePrincipalBot.permissions.has("MANAGE_WEBHOOKS")       ? message += ":orange_square: MANAGE_WEBHOOKS \n"       :     message += ":blue_square: MANAGE_WEBHOOKS \n"
     rolePrincipalBot.permissions.has("MANAGE_EMOJIS")         ? message += ":orange_square: MANAGE_EMOJIS \n"         :     message += ":blue_square: MANAGE_EMOJIS \n"

  if(envoyerMessage && rolePrincipalBot.permissions.has("SEND_MESSAGES") && rolePrincipalBot.permissions.has("EMBED_LINKS")){
    channelPrincipal.send(new Discord.MessageEmbed().setTitle("Permissions du bot").setDescription(message).setFooter("Tant que vous aurez des carrés rouge, le jeu ne se lancera pas.\nSi après avoir changé les droits du role associé au rôle du bot, cet affichage ne change pas, essayez de kick ce dernier puis de le réinviter."));
  }else if(envoyerMessage && rolePrincipalBot.permissions.has("SEND_MESSAGES")){
    channelPrincipal.send("Il faut me rajouter le droit 'EMBED_LINKS' pour obtenir cette information");
  }else if(envoyerMessage){
    console.log("Je ne peut pas envoyer de message, je n'ai pas le droit !");
  }
  return res;
}

/*
UTILITÉ
Explique les commandes à l'utilisateur s'il tape la commande =help
PARAMÈTRE
channel -> le channel où sera envoyer le message
*/
function help(channel){
  let msg = "**=start** : Lance la partie si elle n'est pas déjà en cours\n";
  msg += "**=stop** : Arrête la partie en cours\n";
  msg += "**=regles** : Explication complètes des règles du jeu\n";
  msg += "**=role** : Envoie par message privé ton role avec des explications dessus (nécessite que tu sois dans une partie)\n";
  msg += "**=unmute** : Retire votre mute serveur (dans le cas d'une erreur de la part du bot)\n";
  msg += "**=perms** : Affiche les permissions nécéssaires / manquantes du bot (pour la résolution de problèmes)";
  channel.send(new Discord.MessageEmbed().setTitle("Liste et explication des commandes").setDescription(msg).setColor(couleurEmbedExplications)).then(function(newMessage){setTimeout(function(){newMessage.delete()}, tempsAvantDeleteMessageInformations*10000)});
}

/*
PARAMÈTRE
channelParticulier: Un channel spécifique dans lequel envoyer l'info -> utile pour l'envoyer à une seule personne qui demanderais des infos. Valeur par default sur le salon textuel du village pour donner à tous les règles
UTILITÉ
Epliquer les règles :D (Obvious)
*/
function expliquerRegles(channelParticulier=channelTextuelVillage){
  return new Promise(async function(resolve){
    await channelParticulier.send(new Discord.MessageEmbed().setTitle("Conditions de victoire").addFields(
      { name: "Les Villageois", value: "Ils gagnent en éliminant tous les Loups-Garous."},
      { name: "Les Loups-Garous", value: "Ils gagnent s'ils éliminent le dernier villageois."},
      { name: "Cas spécial", value: "S'il existe deux amoureux dont l'un est Loup-Garou et l'autre villageois, ils gagnent si tous les autres participants meurent."})
      .setColor(couleurEmbedInfos));

    await channelParticulier.send(new Discord.MessageEmbed().setTitle("Étapes d'une journée").setDescription("Une journée se déroule en plusieurs étapes _(ré-expliquées au cours de la partie)_").addFields(
      { name: "Tour 1", value: "‣ Le serveur s'endort\n‣ Appel de cupidon _(si présent)_ puis des amoureux\n‣ Le serveur se réveille"},
      { name: "Tours normaux", value: "‣ Le serveur s'endort\n‣ Les rôles sont appelés 1 par 1 pour effectuer leurs actions\n‣ Le serveur se réveille\n‣ Le serveur débat\n‣ Le serveur vote\n‣ Le serveur se rendort"})
      .setColor(couleurEmbedInfos));

    await channelParticulier.send(new Discord.MessageEmbed().setTitle("Le village")
      .setDescription("Chaque nuit, il est la cible des Loups-Garous. Chaque joueur villageois **dévoré** est éliminé du jeu. Les villageois survivants se réunissent le lendemain matin et essaient de **remarquer**, chez les autres joueurs, **les signes** qui trahiraient leur identité nocturne de mangeur d’homme. Après discussions _(J'ai bien dit **discussion** benjamin)_, ils **votent** contre un suspect qui sera **éliminé** du jeu, jusqu’à la prochaine partie...")
      .addField("Attention", "Sont considérés villageois tous ceux n'ayant pas la carte Loup-garou.")
      .setColor(couleurEmbedInfos));

    for(let [role, map] of lesRoles){
      await expliquerRole(role, channelParticulier);
    }

    resolve();
  });
}
