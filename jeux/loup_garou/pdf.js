//==============================================================================
// Les logs (fin de partie)
//==============================================================================


/*
UTILITÉ
Générer un pdf récapitulatif des différents évenements de la partie pour l'envoyer ensuite dans le tchat
*/
function genererPDFLogs(){
  return new Promise(function(resolve) {
    bot.fs.writeFile(generationPDF+"_evenements.md", texteLogs, async function(err){
      if(err) console.log("generation evenements logs erreur : "+err);

      // finNouvellePage(); // Pour els logs -> Fermer la page de titre
      await pageJoueurs(2); // Pour les logs -> génération de la page des joueurs

      bot.markdownpdf({paperBorder: "1cm", cssPath: "./jeux/loup_garou/logs/style.css", remarkable:{html: true}}).concat.from(["./jeux/loup_garou/logs/exemple/resumePartie_intro.md", generationPDF+"_participants.md", generationPDF+"_stats.md", generationPDF+"_evenements.md"]).to(generationPDF+".pdf", function(){
        resolve();
      });
    });
  });
}

/*
UTILITÉ
Permet d'écrire sur une nouvelle page (ne pas oublier d'appeler finNouvellePage() lors de la fin de l'utilisation)
PARAMÈTRES
titre -> le titre de la page s'il y en a un
sousTitre -> un sous titre pour la page s'il y en a un
*/
function nouvellePage(titre=null, sousTitre=null){
  texteLogs+="<section>";
  if(titre){
    texteLogs+="<center>\n\n# "+titre+"\n";
    if(sousTitre){
      texteLogs+=sousTitre+"\n";
    }
    texteLogs+="</center>\n";
  }
}

/*
UTILITÉ
Ferme la page courante dans laquelle nous écrivons
*/
function finNouvellePage(){
  texteLogs+="</section>\n";
}

/*
UTILITÉ
Ajoute un événement non linéaire au logs avec les paramètres
PARAMÈTRE
liste -> une liste de 2 listes de 2 éléments (image, titre)
*/
function evenementNonLineaire(liste){
  texteLogs+="<ul class='evenement-non-lineaire border-left'>\n";
  for([image, message] of liste){
    texteLogs+="<div>\n";
    espace();
    texteLogs+="<div>\n<img class='icons-carre' alt='icone information' src='./jeux/loup_garou/logs/icons/"+image+"'/>\n"+message+"\n</p>\n</div>\n";
    espace();
    texteLogs+="</div>\n";
  }
  texteLogs+="</ul>\n";
}

/*
UTILITÉ
Ajoute un événement aux logs à partir des paramètres
À utiliser pour les évenements importants (comme les votes, jour, nuit etc...)
*/
function evenement(image="information.svg", titre="Annonce sans titre", style="", explication=null){
  texteLogs+="<div class='evenement "+style+"'>\n<p>\n<img class='icons' alt='icone information' src='./jeux/loup_garou/logs/icons/"+image+"'/>\n"+titre+"\n</p>\n";
  if(explication){
    texteLogs+="<p>"+explication+"</p>";
  }
  texteLogs+="</div>\n";
}

/*
UTILITÉ
Ajoute un événement (avec affichage plus discret) aux logs à partir des paramètres
À utiliser pour les évenements peu important comme les actions des divers roles
*/
function evenementBulle(image="information.svg", titre="Annonce sans titre", style=""){
  texteLogs+="<div class='bulle-colle'>\n<p>\n<img class='icons-bulle "+style+"' alt='icone information' src='./jeux/loup_garou/logs/icons/"+image+"'/>\n"+titre+"\n</p>\n</div>\n\n";
}

/*
UTILITÉ
Ajoute un séparateur pour les événements
*/
function separateur(){
  texteLogs+="\n<div class='evenement-sep'></div>\n\n";
}

/*
UTILITÉ
Ajouter un espace vertical de x cm (0.5, 1 ou 2)
pour 0.5, il faut appeler la fonction avec "05" comme paramètre
*/
function espace(taille="05"){
  texteLogs+="\n<div class='space"+taille+"'></div>\n";
}

/*
UTILITÉ
Générer la page avec la liste des joueurs et de leurs roles
*/
function pageJoueurs(passage){
  if(passage == 1){
    copieLesJoueurs = new Map();
    for(let [user, liste] of lesJoueurs){
      copieLesJoueurs.set(user, liste);
    }
    return ;
  }else if(passage == 2){
    return new Promise(function(resolve, reject){
      let texte = "";
      texte += "<section>\n<center>\n\n# Participants et rôles\nAssociation des rôles aux joueurs\n</center>\n\n<div class='bords max tr'>\n\n| Joueur | Rôle principal | Mort ? | Durée de vie | Rôle(s) secondaire |\n |:---:|:---|:---|:---|:---|\n";

      for(let [user, liste] of copieLesJoueurs){
        let estMort = "oui.png";
        let temps;
        if(lesJoueurs.get(user)){
          estMort = "non.png";
          temps = valeurTimerGlobal;
        }
        else{
          temps = lesMorts.get(user)[3];
        }

        if((capitaine && capitaine == user) && (lesAmoureux.length > 0 && lesAmoureux.includes(user))){ // La personne est capi et amoureuse
          texte += "| "+user.username+" | "+liste[0]+" | <img class='icons-bilan-tableau' alt='icone information' src='./jeux/loup_garou/logs/icons/"+estMort+"'/> | "+Math.floor(temps/60)+" min "+temps%60+" s |Capitaine & Amoureu |\n";
        }else{
          if(capitaine && capitaine == user){ // La parsonne est capi
            texte += "| "+user.username+" | "+liste[0]+" | <img class='icons-bilan-tableau' alt='icone information' src='./jeux/loup_garou/logs/icons/"+estMort+"'/> | "+Math.floor(temps/60)+" min "+temps%60+" s |Capitaine |\n";
          }else if(lesAmoureux.length > 0 && lesAmoureux.includes(user)){ // La personne est Amoureuse
            texte += "| "+user.username+" | "+liste[0]+" | <img class='icons-bilan-tableau' alt='icone information' src='./jeux/loup_garou/logs/icons/"+estMort+"'/> | "+Math.floor(temps/60)+" min "+temps%60+" s |Amoureu |\n";
          }else{ // La personne n'a pas de role secondaire
            texte += "| "+user.username+" | "+liste[0]+" | <img class='icons-bilan-tableau' alt='icone information' src='./jeux/loup_garou/logs/icons/"+estMort+"'/> | "+Math.floor(temps/60)+" min "+temps%60+" s | |\n";
          }
        }
      }
      texte += "\n</div>\n\n</section>";

      bot.fs.writeFile(generationPDF+"_participants.md", texte, function(err){ // Génération du fichier avec la liste des joueurs
        if(err) console.log("generation page participants logs erreur : "+err);
        resolve();
      });
    });
  }
}

/*
UTILITÉ
Générer la page de stats des logs
*/
function pageStats(gagnants){
  return new Promise(function(resolve){
    let texte = "";

    texte += "<section>\n<center>\n\n# Merci d'avoir joué</center>\n";
    texte += "<div class='max'>\n\n|  |  |  |  |\n|:-------:|:-------:|:-------:|:-------:|\n|<img class='icons-bilan' alt='icone information' src='./jeux/loup_garou/logs/icons/soleil.svg'/><p>"+nbJours+" jour(s)</p>|<img class='icons-bilan' alt='icone information' src='./jeux/loup_garou/logs/icons/groupe.svg'/><p>"+copieLesJoueurs.size+" joueur(s)</p>|<img class='icons-bilan' alt='icone information' src='./jeux/loup_garou/logs/icons/trophe.svg'/><p>"+gagnants+"</p>|<img class='icons-bilan' alt='icone information' src='./jeux/loup_garou/logs/icons/lune.svg'/><p>"+nbNuits+" nuit(s)</p>|\n\n</div>\n\n";

    texte+="\n<div class='space1'></div>\n";
    if(gagnants == "perdants"){
      texte += "<center>\n<img style='border-radius: 5px;' alt='image des gagnants' src='./jeux/loup_garou/logs/icons/gagnants_egalite.png'/>\n</center>\n\n";
    }else{
      texte += "<center>\n<img style='border-radius: 5px; height: 12cm;' alt='image des gagnants' src='./jeux/loup_garou/logs/temporaire/gagnants.png'/>\n</center>\n\n";
    }
    texte+="\n<div class='space1'></div>\n";

    clearInterval(timerGlobal);
    texte += "<div class='max'>\n\n|  |  ||  |\n|:-------:|:-------:|:-------:|:-------:|\n| <img class='icons-bilan' alt='icone information' src='./jeux/loup_garou/logs/icons/chrono.svg'/><p>"+Math.floor(valeurTimerGlobal/60)+" min "+valeurTimerGlobal%60+" s</p> | <img class='icons-bilan' alt='icone information' src='./jeux/loup_garou/logs/icons/satisfaction.svg'/><p>Note moyenne: "+noteMoyennePartie+"</p> || <img class='icons-bilan' alt='icone information' src='./jeux/loup_garou/logs/icons/mort.svg'/><p>"+lesMorts.size+" mort(s)</p> |\n\n</div>\n\n";
    texte+="</section>\n";

    bot.fs.writeFile(generationPDF+"_stats.md", texte, function(err){
      if(err) console.log("generation stats logs erreur : "+err);
      resolve();
    });
  });
}

/*
UTILITÉ
Envoyer un message pour demander au joeurs de donner une note de 0 à 5 concernant leur ressenti de la partie en général
PARAMÈTRE
etatVictoire -> (String) pour générer la page de stats en fonction du gagnant de la partie
*/
function notationPartie(etatVictoire){
  setTimeout(function(){
    channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Quelle belle partie !").setDescription("Vous avez apprécié cette partie ? Notez la de 0 à 5 points :thumbsup:").addField("Temps restant", tempsNotationPartie+"s").setColor(couleurEmbedSondage)).then(async function(message){
      let listeReactions = [];
      for(let i=0; i<6; i++){
        listeReactions.push(lesChiffres.get(i));
      }

      let cpt = 0;
      let timer = setInterval(function(){
        message.edit(new Discord.MessageEmbed().setTitle("Quelle belle partie !").setDescription("Vous avez apprécié cette partie ? Notez la de 0 à 5 points :thumbsup:").addField("Temps restant", (tempsNotationPartie-cpt)+"s").setColor(couleurEmbedSondage));
        if(cpt >= tempsNotationPartie){
          clearInterval(timer);
        }
        cpt ++;
      }, 1000);


      const filter = (reaction, user) => listeReactions.includes(reaction.emoji.name) && !user.bot;
      const collector = message.createReactionCollector(filter, { time: tempsNotationPartie*1000});
      let cptMoyenne = 0;

      for(let i=0; i<6; i++){
        await message.react(lesChiffres.get(i)); // Réactions après le collector pour éviter que le temps que toutes les réactions soient mises, les votes des joueurs ne soient pas pris en compte
      }

      collector.on('collect', async function(r, user){
        for (const reac of message.reactions.cache.filter(reac => reac.users.cache.has(user.id)).values()) {
          if(reac != r){
            await reac.users.remove(user.id);
          }
        }
      });

      collector.on('end', async function(collected){
        clearInterval(timer);
        await message.delete();
        if(collected.size > 0){
          let somme = 0;
          let cptMoyenne = 0;
          for (let [emoji, reactionManager] of collected) {
            somme += reactionManager.count;
            switch (emoji){
              case lesChiffres.get(0):
                break;
              case lesChiffres.get(1):
                cptMoyenne += 1 * reactionManager.count;
                break;
              case lesChiffres.get(2):
                cptMoyenne += 2 * reactionManager.count;
                break;
              case lesChiffres.get(3):
                cptMoyenne += 3 * reactionManager.count;
                break;
              case lesChiffres.get(4):
                cptMoyenne += 4 * reactionManager.count;
                break;
              case lesChiffres.get(5):
                cptMoyenne += 5 * reactionManager.count;
                break;
              default:
                console.log("Vote de fin de partie, emoji non reconnu : "+ emoji);
            }
          }
          noteMoyennePartie = cptMoyenne/somme+" / 5";
          await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Merci d'avoir voté").setDescription("La partie étant terminée, tous les channels vont êtres supprimés").addField("Une interrogation ?", "Vous disposerez bientôt d'un résumé de la partie").setColor(couleurEmbedInfos));
        }else{
          noteMoyennePartie = "Pas de vote";
          await channelTextuelVillage.send(new Discord.MessageEmbed().setTitle("Personne n'a voté mais ce n'est pas grave").setDescription("La partie étant terminée, tous les channels vont êtres supprimés").addField("Une interrogation ?", "Vous disposerez bientôt d'un résumé de la partie").setColor(couleurEmbedInfos));
        }

        switch(etatVictoire){
          case "vl": // Victoire des loups
            await pageStats("loups"); // Pour les logs
            break;
          case "vv": // Victoire village
            await pageStats("village"); // Pour les logs
            break;
          case "vp": // Pas de victoire
            await pageStats("perdants"); // Pour les logs
            break;
          case "va": // Victoire des amoureux
            await pageStats("amoureux"); // Pour les logs
            break;
        }

        setTimeout(async function(){
          arreterPartieEncours();
          await genererPDFLogs();
          channelPrincipal.send(new Discord.MessageEmbed().setTitle("Le résumé est arrivé !").attachFiles([generationPDF+".pdf"]).setDescription("Retrouvez tous les détails croustillant de la partie ici :D"));
        }, tempsAnnonceResultatPartie*1000);
      });
    });
  }, 10000); // 10 secondes avant d'avoir le message de notation
}
