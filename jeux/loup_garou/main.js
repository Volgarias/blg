var Discord; // Le lien vers l'API Discord
var Canvas; // La surface de dessin pour les image de fin de partie
var Image; // Le package de dessin pour les iages de fin de partie
var bot; // Le bot en lui-même (Client)
var memberBot; // le memeber correspondant au bot sur la guildeCourante (le serveur actuel de la partie)
var serveur; // Le serveur local servant à la mise en ligne du site avec le tableau des rôles encore en partie

// ******************** BETA
const BETA = true; // Oblige quiconque voulant utiliser le bot à posséder le role manageBLG pour chaque action
// ******************** BETA

// CONSTANTES
const tempsAvantAnnonceRolePersonneElimine = BETA ? 5 : 5; // Secondes -> Entre l'annonce de la personne éliminée et l'affichage de son role
const tempsAvantDeleteMessageInformations  = BETA ? 10 : 20; // Secondes -> Pour les messages d'erreur et informations (sur les rôles par exemple)
const tempsObservationPetiteFille          = BETA ? 20 : 20; // Secondes -> Temps donné à la petite-fille par nuit durant lequel tous les messages des loups lui seront renvoyés par le bot
const tempsAttenteAvantFinSondage          = BETA ? 10 : 30; // Secondes -> Temps laissé pour répondre aux sondages (début de partie, proposition règles)
const tempsAttenteLancementPartie          = BETA ? 10 : 40; // Secondes -> Temps pour lire les infos reçus en MP sur les rôles (avant de commecer avec l'annonce des règles)
const tempsAnnonceResultatPartie           = BETA ? 10 : 10; // Secondes -> Temps d'affichage des gagants de la partie avant la supression des channels/supression des données de partie
const tempsMaxDispoPourVoteLoups           = BETA ? 10 : 30; // Secondes -> Temps disponible pour laisser débatre les loups garous sur le choix d'une victime.
const tempsEliminationChasseur             = BETA ? 10 : 15; // Secondes -> Temps laissé au chasseur pour choisir un joueur
const tempsAvantLancementJour              = BETA ? 5 : 10; // Secondes -> Temps entre la fin de la nuit et le lancement du jour et inversement
const tempsMaxChoixSuccesseur              = BETA ? 10 : 15; // Secondes -> Temps laissé au capitaine lors de sa mort pour désigner un successeur
const tempsElectionCapitaine               = BETA ? 10 : 50; // Secondes -> Temps disponible pour laisser débatre le serveur à propos du choix du Capitaine
const tempsVoteElimination                 = BETA ? 15 : 60; // Secondes -> Temps pour le vote chaque jour
const tempsNotationPartie                  = BETA ? 10 : 15; // Secondes -> Pour le message de fin de partie permettant de noter de 1 à 5 la partie qui viens de se terminer
const tempsChoixSorciere                   = BETA ? 30 : 30; // Secondes -> Temps laissé à la sorcière pour choisir d'utiliser une potion ou non
const tempsChoixCupidon                    = BETA ? 30 : 30; // Secondes -> Temps laissé à cupidon pour choisir deux amoureux
const tempsChoixVoyante                    = BETA ? 20 : 30; // Secondes -> Temps laissé à la voyante pour choisir un joueur
const portServeur                          = 5000; // Le port utilisé pour le serveur local et la communication avec le serveur distant
const couleurEmbedExplications             = "0xecf0f1"; // Blanc
const couleurEmbedSondage                  = "0x16a086"; // Vert
const couleurEmbedInfos                    = "0x297fb8"; // Bleu
const couleurEmbedError                    = "0xb40000"; // Rouge
const generationPDF                        = "./jeux/loup_garou/logs/temporaire/resumePartie"; // Le fichier pdf généré à la fin de la partie
const lesLettres                           = new Map([[0,'🇦'],[1,'🇧'],[2,'🇨'],[3,'🇩'],[4,'🇪'],[5,'🇫'],[6,'🇬'],[7,'🇭'],[8,'🇮'],[9,'🇯'],[10,'🇰'],[11,'🇱'],[12,'🇲'],[13,'🇳'],[14,'🇴'],[15,'🇵'],[16,'🇶'],[17,'🇷'],[18,'🇸'],[19,'🇹'],[20,'🇺'],[21,'🇻'],[22,'🇼'],[23,'🇽'],[24,'🇾'],[25,'🇿']]); // Dictionnaire avec (int)index -> (String)ième lettre de l'alphabet sous forme d'emoji
const lesChiffres                          = new Map([[0,'0️⃣'],[1,'1️⃣'],[2,'2️⃣'],[3,'3️⃣'],[4,'4️⃣'],[5,'5️⃣']]); // Dictionnaire avec (int)index -> (String)numero  sous forme d'emoji
const lesRoles                             = new Map([["Petite-fille", new Map([
                                                        ["special", false],
                                                        ["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Petite-fille.png"],
                                                        ["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Petite-fille.png"]
                                                      ])],
                                                      ["Chasseur", new Map([
                                                        ["special", false],
                                                        ["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Chasseur.png"],
                                                        ["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Chasseur.png"]
                                                      ])],
                                                      ["Cupidon", new Map([
                                                        ["special", false],
                                                        ["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Cupidon.png"],
                                                        ["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Cupidon.png"]
                                                      ])],
                                                      ["Sorciere", new Map([
                                                        ["special", false],
                                                        ["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Sorciere.png"],
                                                        ["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Sorciere.png"]
                                                      ])],
                                                      ["Voyante", new Map([
                                                        ["special", false],
                                                        ["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Voyante.png"],
                                                        ["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Voyante.png"]
                                                      ])],
                                                      ["Loup-garou", new Map([
                                                        ["special", true],
                                                        ["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Loup-garou.png"],
                                                        ["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Loup-garou.png"]
                                                      ])],
                                                      ["Villageois", new Map([
                                                        ["special", true],
                                                        ["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Villageois.png"],
                                                        ["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Villageois.png"]
                                                      ])],
                                                      ["Capitaine", new Map([
                                                        ["special", true],
                                                        ["original", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Capitaine.png"],
                                                        ["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Capitaine.png"]
                                                      ])],
                                                      ["Medium", new Map([
                                                        ["special", false],
                                                        ["original","https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/originales/Medium.png"],
                                                        ["custom", "https://gitlab.com/Volgarias/blg/-/raw/master/jeux/loup_garou/images_roles/custom/Medium.png"]
                                                      ])]]); // Association des rôles à leurs images sur Google drive ainsi qu'a un boolean qui nous dit si le role est spécial comme le loupg garou et villageois qui peut-être donné plusieurs fois et le capitaine qui est un role secondaire

// CHANNELS
var channelTextuelLoupsGarous; // Le channel textuel réservé aux loups-garous
var channelTextuelPetiteFille; // Le channel textuel réservé à la patite-fille
var channelTextuelMediumMort; // Le channel textuel réservé au médium (envois des messages des morts)
var channelTextuelSorciere; // Le channel textuel réservé à la socière
var channelTextuelCupidon; // Le channel textuel réservé à Cupidon
var channelTextuelVoyante; // Le channel textuel réservé à la voyante
var channelTextuelVillage; // Le channel dans lequel raconter l'histoire
var channelTextuelMedium; // Le channel textuel réservé au médium (réception du message)
var channelVocalVillage; // Le channel vocal dans lequel se trouve tout le monde
var categoryChannelLG; // La catégorie pour les channels (textuel et vocaux)
var channelPrincipal; // Le channel textuel ou le message de démarrage à été envoyé
var guildeCourante; // Le serveur sur lequel la partie est en cours

// ROLES
var lesAmoureux = []; // Liste des amoureux (User)
var rolePrincipalBot; // Le role automatiquement assigné au bot
var petiteFille; // La petite fille (User) si rôle existant
var capitaine; // Le capitaine de la partie (User)
var chasseur; // Le chasseur (User) si rôle existant
var sorciere; // La sorcière (User) si rôle existant
var cupidon; // Cupidon (User) si rôle existant
var voyante; // La voyante (User) si rôle existant
var medium; // Le medium (User) si rôle existant

// LOGS
var copieLesJoueurs; // Copie de la map lesJoueurs (copie effectuée au début de la partie et ne sauvegardant pas l'avancée de la partie)
var timerGlobal; // Chronomètre pour le temps de la partie
var noteMoyennePartie; // La note pour la partie qui vient de se finir et déterminée en fonction du cote des joeurs
var infoPartie                            = {}; // Les informations sur les role participants à la partie qui seront inscrites dans le fichier infoPartie.json
var [nbJours, nbNuits, valeurTimerGlobal] = [0, 0, 0]; // Compteurs pour le nb de jours et de nuits ainsi que pour le temps de la partie
var texteLogs                             = ""; // String contenant tout les évenements du jeux avec une certaine mise en forme pour faire un compte rendu à la fin
var messagesTemporaires                   = []; // Liste pour stocker des messages le temps de l'execution d'in fonction pour ensuite mettre ces messages dans les logs

// AUTRES
var connectionVocaleBot; // Stocke la connection au channel vocal du village pour pouvoir jouer les sons de la partie
var partieEnCours        = false; // Boolean pour savoir si une partie est lancée ou non
var listeLoups           = []; // La liste des loups de la partie (User)
var listePotionsSorciere = [true, true]; // La liste des booleans indiquant si la potion à été utilisé au court de la partie. Ordre des potions : guérison, empoisonement
var mortsDeLaNuit        = []; // La liste contenant la ou les personnes tuées durant la nuit courante en attente de l'annonce de leur mort. (User)
var lesJoueurs           = new Map(); // Dictionnaire avec (User)user -> [ (String)role , (Role) role , (GuildMember) member]
var fonctionStop         = new Map([["roles", []], ["salons", []], ["messages", []]]); // Dictionnaire pour la supression des channels, des rôles etc...
var lesMorts             = new Map(); // Dictionnaire avec (User)user -> [(String)surnom, (Role) role, (GuildMember)member, (number)tempsVie]
var lienSite             = ""; // Le string contenant le lien de la page mise en ligne pour le tableau des rôles


//==============================================================================
// Initialisation du jeu
//==============================================================================


/*
UTILITÉ
Initialise les variables du fichier
PARAMÈTRES
robot   -> le bot discord
discord -> la librairie discord
canvas  -> une bibliothèque pour le traitement d'images
image   -> une bibliothèque pour le traitement d'images
*/
function initLoup_garou(robot, discord, canvas, image){
  bot = robot;
  bot.vm.runInThisContext(bot.fs.readFileSync("./jeux/loup_garou/commandes.js"));
  bot.vm.runInThisContext(bot.fs.readFileSync("./jeux/loup_garou/roles.js"));
  bot.vm.runInThisContext(bot.fs.readFileSync("./jeux/loup_garou/pdf.js"));
  bot.vm.runInThisContext(bot.fs.readFileSync("./jeux/loup_garou/tableau.js"));
  bot.vm.runInThisContext(bot.fs.readFileSync("./jeux/loup_garou/deroulement.js"));
  Discord = discord;
  Canvas = canvas;
  Image = image;
}
