const Discord = require('discord.js');
const bot = new Discord.Client();
const { Canvas, Image } = require('canvas');

bot.vm = require('vm');
bot.markdownpdf = require('markdown-pdf');
bot.mergeImages = require('merge-images');
bot.ngrok = require('ngrok');
bot.express = require('express');
bot.fs = require('fs');
bot.ytdl = require('ytdl-core');

bot.vm.runInThisContext(bot.fs.readFileSync("./jeux/loup_garou/main.js")); // Intégration du fichier loup_garou

//==============================================================================
//listeners
//==============================================================================

bot.on('ready', function(){ //lors du lancement du bot
  bot.user.setActivity('attendre lundi soir :D').catch(console.error); //définir son activité (message en dessous de son pseudo)
  // bot.user.setAvatar('./logo.png'); // mettre une image de profil au bot
  initLoup_garou(bot, Discord, Canvas, Image); //Initialisation du fichier loup_garou.js
  console.log("======== Je suis prêt ========\nPenser à enlever les rôles qui peuvent bouger et mute/unmute les joueurs\n==============================");
});

bot.on('message', function(message){ //lorsqu'un message est envoyé
  if(message.content.startsWith('=')) {
    commandeLoupGarou(message, bot, Discord);
  }
});

bot.on('guildCreate', async function(guild){
  await guild.roles.create({data: { name: 'ManageBLG' },reason: 'Creation du rôle de gestion du bot'});
});

//==============================================================================
//fonctions
//==============================================================================

bot.randomItemFromListe = function(liste){
  return liste[Math.floor(Math.random() * liste.length)];
}

//==============================================================================
//connexion
//==============================================================================

bot.login(JSON.parse(bot.fs.readFileSync("./token.json")).token); //log le bot sur discord
