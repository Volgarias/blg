## Installation
À la racine du projet, tapez les commandes :
```bash
npm install --safe discord.js // Pour Discord API
npm install ytdl-core // Pour lire des musiques en vocal
npm install @discordjs/opus // Pour lire des musiques en vocal
npm install markdown-pdf // Pour générer un pdf de logs à la fin de la partie
npm install merge-images // Pour fusionner plusieurs images
npm install canvas // Pour utiliser le module du dessus avec node.js
npm install ngrok // Pour le tableau des rôles en ligne
npm install express // Pour le tableau des rôles en local
npm install dotenv // Pour les variables d'environement tel que le token et le mode de test
```

#### Peut être nécéssaire
```bash
npm install node-opus // Pour lire des musiques en vocal
npm install ffmpeg-static // Pour la résoolution d'une potentielle erreure
```

## Lancement
Il faut d'abord que vous crééiez un bot auprès de discord pour récupérer son token (son identifiant de connexion auprès de Discord).
Une fois le token récupéré, il vous faut créer un fichier nommé `token.json` à la racine du projet ayant ce contenu:
```json
{
    "token": "METTEZ VOTRE TOKEN ICI"
}
```

Maintenant que les modules sont installés et que vous avez récupéré votre token, tapez:
```bash
node index.js
```
pour lancer le bot

## Résolution de problèmes probables
#### Flat avec les Embed messages
Pour résoudre cela, allez dans `spacebot/node_modules/discord.js/src/structures/MessageEmbed.js` à la ligne `434` et remplacer la fonction suivante
```js
static normalizeFields(...fields) {
    return fields
      .flat(2)
      .map(field =>
        this.normalizeField(
          field && field.name,
          field && field.value,
          field && typeof field.inline === 'boolean' ? field.inline : false,
        ),
      );
  }
```
Par celle-ci
```js
static normalizeFields(...fields) {
    return fields
      .reduce((acc, val) => acc.concat(val), [])
      .map(field =>
        this.normalizeField(
          field && field.name,
          field && field.value,
          field && typeof field.inline === 'boolean' ? field.inline : false,
        ),
      );
  }
```
